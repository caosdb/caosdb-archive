/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.mpg.de.jheartdb;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * 
 * @author Julian
 */
public class FileClient {
    // TODO

    private static String downloadFilesTo = "/FILESERVER/Downloads/HeartDB/";

    /**
     * If there is a file in the Dropoffbox this method picks the file up and
     * puts it in the system.
     * 
     * @param urlstring
     * @param usrname
     * @param usrid
     * @param folder
     * @param file
     * @return
     */
    public static String pickup(String urlstring, String usrname, String usrid, String folder,
            String file, String password) {
        urlstring += "FilesDropOf";
        try {
            HttpPost post = new HttpPost(urlstring);
            post.setHeader("USRNAME", usrname);
            post.setHeader("USRID", usrid);
            post.setHeader("FILENAME", file);
            post.setHeader("FOLDERNAME", folder);
            post.setHeader("PASSWORD", password);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(post);
            HttpEntity responseBody = response.getEntity();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            responseBody.writeTo(stream);
            return stream.toString();
        } catch (Exception e) {
            System.out.println("Exception Communication.pickup(String urlstring, "
                    + "String usrname, String usrid,String folder, String file, String password).");
            return null;
        }
    }

    /**
     * Sends a whole folder to the system. It first creates a folder and than
     * posts the underlieng files to the folder.
     * 
     * @param urlstring
     *            url of the server
     * @param usrname
     *            must be entered to the database and needs a filesystem
     * @param usrid
     * @param folder
     *            relative path where you want to store the file
     * @param file
     *            absolute path of the file to transmit
     * @return
     */
    public static String postFolder(String urlstring, String usrname, String usrid, String folder,
            File file, String password) {
        String response = "";
        folder += "/" + file.getName();
        createFolder(urlstring, usrname, usrid, folder, password);
        response += "Created folder: " + folder + "\n";

        File[] entries = file.listFiles();
        for (File e : entries) {
            if (!e.isDirectory()) {

                if (postFile(urlstring, usrname, usrid, e, "", folder, password, "")) {
                    response += "Submitted file: " + folder + e.getName() + "\n";
                } else {
                    response += "Error: tried to submit: " + folder + e.getName() + "\n";
                }
            } else {
                postFolder(urlstring, usrname, usrid, folder, e, password);
            }
        }

        return response;
    }

    /**
     * Creates a folder at the filesystem.
     * 
     * @param urlstring
     *            url of the server
     * @param usrname
     *            must be entered to the database and needs a filesystem
     * @param usrid
     * @param folder
     *            the relative path of the folder you want to create
     * @return
     */
    public static String createFolder(String urlstring, String usrname, String usrid,
            String folder, String password) {
        urlstring += "Files";
        try {
            HttpPut putFolder = new HttpPut(urlstring);
            putFolder.setHeader("USRNAME", usrname);
            putFolder.setHeader("USRID", usrid);
            putFolder.setHeader("FILENAME", folder);
            putFolder.setHeader("PASSWORD", password);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(putFolder);
            HttpEntity responseBody = response.getEntity();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            responseBody.writeTo(stream);
            return stream.toString();
        } catch (Exception e) {
            System.out
                    .println("Exception Communication.createFolder(String urlstring, String usrname, String usrid, String folder, String password).");
            return null;
        }
    }

    /**
     * Searches the filesystem for the files of the user. Returns a xml file
     * with the hierarchical filesystem.
     * 
     * @param urlstring
     *            url of the server
     * @param usrname
     *            must be entered to the database and needs a filesystem
     * @param usrid
     * @param password
     * @return
     */
    public static String showFiles(String urlstring, String usrname, String usrid, String password) {
        urlstring += "Files_USR";
        try {
            HttpGet filesGet = new HttpGet(urlstring);
            filesGet.setHeader("USRNAME", usrname);
            filesGet.setHeader("USRID", usrid);
            filesGet.setHeader("PASSWORD", password);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(filesGet);
            HttpEntity responseBody = response.getEntity();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            responseBody.writeTo(stream);
            return stream.toString();
        } catch (Exception e) {
            System.out
                    .println("Exception Communication.showFiles(String urlstring, String usrname, String usrid, String password).");
            return null;
        }
    }

    /**
     * Downloads a file from the filesystem.
     * 
     * @param urlstring
     *            url of the server
     * @param usrname
     *            must be entered to the database and needs a filesystem
     * @param usrid
     * @param filename
     *            relativ path of the file
     * @param password
     * @return
     */
    public static String getFile(String urlstring, String usrname, String usrid, String filename,
            String password) {
        urlstring += "Files";
        try {
            HttpGet fileGet = new HttpGet(urlstring);
            fileGet.setHeader("USRNAME", usrname);
            fileGet.setHeader("USRID", usrid);
            fileGet.setHeader("FILENAME", filename);
            fileGet.setHeader("PASSWORD", password);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(fileGet);
            int status = response.getStatusLine().getStatusCode();
            if (status == 200) {
                InputStream is = response.getEntity().getContent();
                String copyTo = "";
                boolean repaired = false;
                for (int i = filename.length() - 1; i >= 0; i--) {

                    if (filename.charAt(i) == '_' && !repaired) {
                        repaired = true;
                        continue;
                    }
                    if (repaired) {
                        copyTo = filename.charAt(i) + copyTo;
                    }
                }
                OutputStream os = new FileOutputStream(downloadFilesTo + copyTo);

                byte[] buffer = new byte[1024];
                int numRead;
                while ((numRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, numRead);
                }
                os.close();

                return "Download successful.\n\rWritten to:" + downloadFilesTo + copyTo;
            } else if (status == 401) {
                return "Download not successful.\n\rPlease check your username, userid and password.";
            } else if (status == 404) {
                return "Download not successful.\n\rFile unknown.";
            } else {
                return "Download not successful.\n\rUnknown Error.\n\rError: " + status;
            }
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    /**
     * Submit a file to the filesystem.
     * 
     * @param urlstring
     *            url of the server
     * @param usrname
     *            must be entered to the database and needs a filesystem
     * @param usrid
     * @param f
     *            absolute path of the file at the users computer
     * @param description
     *            a description of the file. It will be entered to the database
     * @param folder
     *            relative path you want to save the file to
     * @return
     */
    public static String postFile(String urlstring, String usrname, String usrid, File f,
            String description, String folder, String password) {
        urlstring += "Files";
        try {
            String checksumString = getchecksum(f);

            FileInputStream fileInputStream = new FileInputStream(f);
            InputStreamBody inputStreamBody = new InputStreamBody(fileInputStream,
                    "application/octet-stream", f.getName());
            StringBody stringBody = new StringBody("String to upload", Charset.forName("UTF-8"));
            StringBody comment = new StringBody(description, Charset.forName("UTF-8"));
            StringBody rights = new StringBody("wrwr", Charset.forName("UTF-8"));
            StringBody checksum = new StringBody(checksumString, Charset.forName("UTF-8"));

            MultipartEntity multipartEntity = new MultipartEntity();

            multipartEntity.addPart("Stringpart", stringBody);
            multipartEntity.addPart("Comment", comment);
            multipartEntity.addPart("Rights", rights);
            multipartEntity.addPart("Checksum", checksum);

            multipartEntity.addPart("fileToUpload", inputStreamBody);
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(urlstring);
            httpPost.setEntity(multipartEntity);
            httpPost.addHeader("USRNAME", usrname);
            httpPost.addHeader("USRID", usrid);
            httpPost.addHeader("FILENAME", f.getName());
            httpPost.addHeader("FOLDERNAME", folder);
            httpPost.addHeader("PASSWORD", password);

            HttpResponse response = httpClient.execute(httpPost);
            System.out.println(response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            return EntityUtils.toString(entity);
        } catch (Exception e) {
            System.out.println("Exception: [1] Communication.postFile(...)");
            return null;
        }
    }

    public static boolean postFile(String uri, String username, String userid, String password,
            String description, String filename, int size) {
        uri += "Files";
        try {
            System.out.println("Size: " + size);
            System.out.println("filename: " + filename);
            System.out.println("description: " + description);
            System.out.println("password: " + password);
            System.out.println("userid: " + userid);
            System.out.println("username: " + username);
            System.out.println("URI: " + uri);

            class MyFileInputStream extends InputStream {

                int size = 0;
                int pos = 0;
                String checksum = null;
                MessageDigest md = null;

                public MyFileInputStream(int size) throws Exception {
                    md = MessageDigest.getInstance("MD5");
                    this.size = size;
                }

                public String getChecksum() {
                    if (checksum != null) {
                        return checksum;
                    }
                    checksum = "";
                    byte[] result = md.digest();
                    for (int i = 0; i < result.length; i++) {
                        checksum += toHexString(result[i]);
                    }
                    return checksum;
                }

                // @Override
                // public int read(byte[] b) throws IOException {
                // return read(b, 0, b.length);
                // }
                //
                // @Override
                // public int read(byte[] b, int off, int len) throws
                // IOException {
                // int i = off;
                // for (; i < len; i++) {
                // b[i] = (byte) read();
                // }
                // return i - off;
                // }
                @Override
                public int read() throws IOException {
                    if (pos < size) {
                        byte input = 0;
                        md.update(input);
                        pos++;
                        return 0;
                    }
                    return -1;
                }
            }

            MyFileInputStream fileInputStream = new MyFileInputStream(size);
            MyFileInputStream fileInputStreamChecksumMaker = new MyFileInputStream(size);
            System.out.println("Hier2");
            while (fileInputStreamChecksumMaker.read() != -1) {
            }
            System.out.println("Hier3");
            InputStreamBody inputStreamBody = new InputStreamBody(fileInputStream,
                    "application/octet-stream", filename);
            StringBody stringBody = new StringBody("String to upload", Charset.forName("UTF-8"));
            StringBody comment = new StringBody(description, Charset.forName("UTF-8"));
            StringBody rights = new StringBody("wrwr", Charset.forName("UTF-8"));

            StringBody checksum = new StringBody(fileInputStreamChecksumMaker.getChecksum(),
                    Charset.forName("UTF-8"));

            MultipartEntity multipartEntity = new MultipartEntity();

            multipartEntity.addPart("Stringpart", stringBody);
            multipartEntity.addPart("Comment", comment);
            multipartEntity.addPart("Rights", rights);
            multipartEntity.addPart("Checksum", checksum);

            multipartEntity.addPart("fileToUpload", inputStreamBody);
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(uri);
            httpPost.setEntity(multipartEntity);
            httpPost.addHeader("USRNAME", username);
            httpPost.addHeader("USRID", userid);
            httpPost.addHeader("FILENAME", filename);
            httpPost.addHeader("PASSWORD", password);

            HttpResponse response = httpClient.execute(httpPost);
            if (fileInputStreamChecksumMaker.getChecksum().equals(fileInputStream.getChecksum())) {
                System.out.println("Checksum ok");
            } else {
                System.out.println("Checksum not ok");
            }
            if (response.getStatusLine().getStatusCode() == 200) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Exception: [1] Communication.postFile(...)");
            return false;
        }
    }

    public static boolean postFile(String urlstring, String usrname, String usrid, File f,
            String description, String folder, String password, String emptyString) {
        urlstring += "Files";
        try {
            String checksumString = getchecksum(f);

            FileInputStream fileInputStream = new FileInputStream(f);
            InputStreamBody inputStreamBody = new InputStreamBody(fileInputStream,
                    "application/octet-stream", f.getName());
            StringBody stringBody = new StringBody("String to upload", Charset.forName("UTF-8"));
            StringBody comment = new StringBody(description, Charset.forName("UTF-8"));
            StringBody rights = new StringBody("wrwr", Charset.forName("UTF-8"));
            StringBody checksum = new StringBody(checksumString, Charset.forName("UTF-8"));

            MultipartEntity multipartEntity = new MultipartEntity();

            multipartEntity.addPart("Stringpart", stringBody);
            multipartEntity.addPart("Comment", comment);
            multipartEntity.addPart("Rights", rights);
            multipartEntity.addPart("Checksum", checksum);

            multipartEntity.addPart("fileToUpload", inputStreamBody);
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(urlstring);
            httpPost.setEntity(multipartEntity);
            httpPost.addHeader("USRNAME", usrname);
            httpPost.addHeader("USRID", usrid);
            httpPost.addHeader("FILENAME", f.getName());
            httpPost.addHeader("FOLDERNAME", folder);
            httpPost.addHeader("PASSWORD", password);

            HttpResponse response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == 200) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            System.out.println("Exception: [3] Communication.postFile(...)");
            return false;
        }
    }

    /**
     * Creates a filesystem for the user.
     * 
     * @param urlstring
     *            url of the server
     * @param usrname
     *            must be entered to the database and needs a filesystem
     * @param usrid
     * @param password
     * @return
     */
    public static String postUSR(String urlstring, String usrname, String usrid, String password) {
        urlstring += "Files_USR";
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost method = new HttpPost(urlstring);
            method.addHeader("username", usrname);
            method.addHeader("userid", usrid);
            method.addHeader("password", password);
            HttpResponse response = client.execute(method);
            HttpEntity entity = response.getEntity();
            // Get the server status.
            // System.out.println(response.getStatusLine().getStatusCode());
            String result = EntityUtils.toString(entity);
            return result;
        } catch (Exception ex) {
            return "Error:\n\r" + ex.getMessage();
        }
    }

    private static String getchecksum(File f) {
        String checksumString = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            FileInputStream in = new FileInputStream(f);

            int len;
            byte[] data = new byte[1024];
            while ((len = in.read(data)) > 0) {
                md.update(data, 0, len);
            }
            in.close();
            byte[] result = md.digest();
            for (int i = 0; i < result.length; ++i) {
                checksumString += toHexString(result[i]);
            }
        } catch (Exception e) {
            return "error";
        }
        return checksumString;
    }

    // TODO: Check this implementation, it is now also used in DBFrame!
    public static String toHexString(byte b) {
        int value = (b & 0x7F) + (b < 0 ? 128 : 0);
        String ret = (value < 16 ? "0" : "");
        ret += Integer.toHexString(value).toUpperCase();
        return ret;
    }
}
