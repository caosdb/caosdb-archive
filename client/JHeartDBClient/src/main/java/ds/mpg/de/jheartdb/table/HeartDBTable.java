/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-14
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.mpg.de.jheartdb.table;

import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.Record;
import ds.mpg.de.jheartdb.structures.RecordType;
import ds.mpg.de.jheartdb.structures.File;
import java.awt.FontMetrics;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author salexan
 */
public class HeartDBTable extends JTable {

    public HeartDBTable() {
        setModel(new DefaultTableModel(new String[]{}, 0));
        setAutoResizeMode(AUTO_RESIZE_OFF);
    }

    public void setRecords(List<Record> records) {
        setModel(new RecordTableModel(records));
        updateColumnWidths();
    }

    public void setRecordTypes(List<RecordType> records) {
        setModel(new RecordTypeTableModel(records));
        updateColumnWidths();
    }

    public void setPropertyList(List<Property> records) {
        setModel(new PropertyTableModel(records));
        updateColumnWidths();
    }
    
    public void setFileList(List<File> records) {
        setModel(new FileTableModel(records));
        updateColumnWidths();
    }

    public void updateColumnWidths() {
        DefaultTableModel model = (DefaultTableModel) getModel();
        for (int i = 0; i < model.getColumnCount(); i++) {
            String str = model.getColumnName(i);
            FontMetrics f = getFontMetrics(getFont());
            int max = f.stringWidth(str);
            for (int j = 0; j < model.getRowCount(); j++) {
                if (model.getValueAt(j, i) != null) {
                    max = Math.max(max, f.stringWidth(model.getValueAt(j, i).toString()));
                }
            }
            getColumnModel().getColumn(i).setPreferredWidth(Math.min(180, max + 24));
        }
    }
}
