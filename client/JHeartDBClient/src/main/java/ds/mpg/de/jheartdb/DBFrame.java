/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-16
 */
package ds.mpg.de.jheartdb;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FileUtils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import ds.mpg.de.jheartdb.dialog.AddPropertyDialog;
import ds.mpg.de.jheartdb.dialog.DropOffBoxFilePickupDialog;
import ds.mpg.de.jheartdb.dialog.NewRecordDialog;
import ds.mpg.de.jheartdb.dialog.NewRecordTypeDialog;
import ds.mpg.de.jheartdb.log.HTTPLogEntry;
import ds.mpg.de.jheartdb.structures.Info;
import ds.mpg.de.jheartdb.table.HeartDBTable;
import ds.mpg.de.jheartdb.utils.DBStats;
import ds.mpg.de.jheartdb.utils.Utilities;

/**
 * Frame to manage the database.
 * 
 * @author salexan
 */
public class DBFrame extends javax.swing.JFrame {

    private HeartDBTable table = new HeartDBTable();
    private HeartDBClient heartDBClient = HeartDBClient.getInstance();
    private Preferences prefs = Preferences.userNodeForPackage(DBFrame.class);
    private RSyntaxTextArea jtaResp = new RSyntaxTextArea();
    private String password = "";

    /**
     * Creates new form DBFrame
     */
    public DBFrame() {
        initComponents();
        jScrollPane1.setViewportView(table);
        loadPreferences();
        try {
            displayInformation();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null,
                    "An exception ocurred. Please check your connection settings.");
        }
        updateLog();
        // jlLogEntries.setModel(new LogListModel(dbManager.getLogList()));
        jtaResp.setEditable(false);
        jtaResp.setAntiAliasingEnabled(true);
        jtaResp.setLineWrap(false);
        jtaResp.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
        FileNameExtensionFilter filterXML = new FileNameExtensionFilter("XML", "XML");
        jFileChooser1.setFileFilter(filterXML);
        logScrollPane.setViewportView(jtaResp);
    }

    public void updateLog() {
        DefaultListModel model = new DefaultListModel();
        for (HTTPLogEntry l : heartDBClient.getLogList()) {
            model.addElement(l);
        }
        jlLogEntries.setModel(model);
    }

    /**
     * Load preferences from user prefs.
     */
    public final void loadPreferences() {
        jtfGenerator.setText(prefs.get("defaultGeneratorString", "User: , Client: JHeartDB"));
        jtfHostname.setText(prefs.get("hostname", "localhost"));
        jtfPort.setText(prefs.get("port", "8122"));
        jtfBasepath.setText(prefs.get("basepath", "mpidsserver"));
        jtfDownloadFolder.setText(prefs.get("downloadFolder", System.getProperty("user.home")));
        password = prefs.get("password", password);
        jtfUsername.setText(prefs.get("username", ""));
        DBStats.getInstance().setRequestsTotal(prefs.getLong("timeRequestsTotal", 0),
                prefs.getLong("numberRequestsTotal", 0));
        configure();
        updateStats();
    }

    /**
     * Creates an SHA-512 hashed string from a plain text string.
     * 
     * @param password
     * @return
     */
    @Deprecated
    private String hashPassword(String password) {
        try {
            MessageDigest m = MessageDigest.getInstance("SHA-512");
            byte[] pw = m.digest(password.getBytes());
            StringBuilder str = new StringBuilder();
            for (byte b : pw) {
                str.append(FileClient.toHexString(b));
                // str.append(Integer.toHexString(((int)b)+127));
            }
            return str.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DBFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Save preferences to user prefs.
     */
    public void savePreferences() {
        prefs.put("hostname", jtfHostname.getText());
        prefs.put("port", jtfPort.getText());
        prefs.put("basepath", jtfBasepath.getText());
        prefs.put("downloadFolder", jtfDownloadFolder.getText());
        prefs.putLong("timeRequestsTotal", DBStats.getInstance().getTimeRequestsTotal());
        prefs.putLong("numberRequestsTotal", DBStats.getInstance().getnRequestsTotal());
        prefs.put("defaultGeneratorString", jtfGenerator.getText());
        prefs.put("username", jtfUsername.getText());
        // Create Hash and save password:
        String pw = new String(jtfPassword.getPassword());
        if (pw.length() > 0) {
            password = pw;
            jtfPassword.setText("");
        }
        // prefs.put("password", password);
        configure();
    }

    public void updateStats() {
        DBStats stats = DBStats.getInstance();
        jtfTotalNumber.setText(Long.toString(stats.getnRequestsTotal()));
        jtfTotalTime.setText(Long.toString(stats.getTimeRequestsTotal()));
        jtfMeanTime.setText(Double.toString(stats.getMeanRequestsTotal()));
    }

    private void configure() {
        heartDBClient.configureConnection(jtfHostname.getText(),
                Integer.parseInt(jtfPort.getText()), jtfBasepath.getText(), jtfUsername.getText(),
                password);
        if (password.length() <= 0) {
            jlHashedPassword.setText("No password set.");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed"
    // desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jButton5 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jbPostXML = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jtfHostname = new javax.swing.JTextField();
        jtfPort = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jtfBasepath = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jtfDownloadFolder = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jtfGenerator = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jtfUsername = new javax.swing.JTextField();
        jtfPassword = new javax.swing.JPasswordField();
        jLabel16 = new javax.swing.JLabel();
        jlHashedPassword = new javax.swing.JLabel();
        jButton10 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jtfTotalNumber = new javax.swing.JTextField();
        jtfTotalTime = new javax.swing.JTextField();
        jtfMeanTime = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jButton6 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jlLogEntries = new javax.swing.JList();
        jButton9 = new javax.swing.JButton();
        logScrollPane = new javax.swing.JScrollPane();

        jFileChooser1.setDragEnabled(true);
        jFileChooser1.setPreferredSize(new java.awt.Dimension(517, 600));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JHeartDB");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel5.setText("Query entries from:");

        jTextField5.setColumns(5);
        jTextField5.setText("1");

        jLabel6.setText("to:");

        jTextField6.setColumns(5);
        jTextField6.setText("7");
        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(jPanel6Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel6Layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 118,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 127,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(247, Short.MAX_VALUE)));
        jPanel6Layout.setVerticalGroup(jPanel6Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel6Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)
                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE)));

        jLabel3.setText("Query entry list:");

        jTextField3.setText("1&2&7");
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(jPanel5Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel5Layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3)
                        .addGap(39, 39, 39)
                        .addComponent(jTextField3, javax.swing.GroupLayout.DEFAULT_SIZE, 538,
                                Short.MAX_VALUE)));
        jPanel5Layout.setVerticalGroup(jPanel5Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel5Layout
                        .createSequentialGroup()
                        .addGroup(
                                jPanel5Layout
                                        .createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField3,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3)).addGap(0, 12, Short.MAX_VALUE)));

        jLabel7.setText("Query Type:");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Record",
                "RecordType", "Property", "File" }));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel4Layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel7)
                        .addGap(67, 67, 67)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 523, Short.MAX_VALUE)));
        jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel4Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE)));

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setText("OK");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout
                .setHorizontalGroup(jPanel1Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                jPanel1Layout
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(
                                                jPanel1Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(
                                                                jScrollPane1,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                798, Short.MAX_VALUE)
                                                        .addComponent(
                                                                jPanel4,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                Short.MAX_VALUE)
                                                        .addGroup(
                                                                jPanel1Layout
                                                                        .createSequentialGroup()
                                                                        .addComponent(
                                                                                jPanel5,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                Short.MAX_VALUE)
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(
                                                                                jButton1,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                88,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(
                                                                jPanel1Layout
                                                                        .createSequentialGroup()
                                                                        .addComponent(
                                                                                jPanel6,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                Short.MAX_VALUE)
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(
                                                                                jButton3,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                88,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                javax.swing.GroupLayout.Alignment.TRAILING,
                jPanel1Layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(
                                jPanel1Layout
                                        .createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jPanel6,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(
                                jPanel1Layout
                                        .createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanel5,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 442,
                                Short.MAX_VALUE).addContainerGap()));

        jTabbedPane1.addTab("HeartDB Query", jPanel1);

        jButton5.setText("New Record");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton7.setText("New RecordType");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setText("New Property");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton11.setText("Pickup files from DropOffBox");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jbPostXML.setText("POST XML");
        jbPostXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbPostXMLActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout
                .setHorizontalGroup(jPanel3Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                jPanel3Layout
                                        .createSequentialGroup()
                                        .addGroup(
                                                jPanel3Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(
                                                                jPanel3Layout
                                                                        .createSequentialGroup()
                                                                        .addContainerGap()
                                                                        .addComponent(
                                                                                jbPostXML,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                225,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                jPanel3Layout
                                                                        .createSequentialGroup()
                                                                        .addGap(269, 269, 269)
                                                                        .addGroup(
                                                                                jPanel3Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                        .addComponent(
                                                                                                jButton5,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                225,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                        .addComponent(
                                                                                                jButton7,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                225,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                        .addComponent(
                                                                                                jButton8,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                225,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                        .addComponent(
                                                                                                jButton11,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                225,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addContainerGap(328, Short.MAX_VALUE)));
        jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel3Layout.createSequentialGroup().addGap(123, 123, 123).addComponent(jbPostXML)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton11).addGap(0, 292, Short.MAX_VALUE)));

        jTabbedPane1.addTab("HeartDB Edit", jPanel3);

        jLabel1.setText("Hostname:");

        jLabel2.setText("Port:");

        jtfHostname.setText("localhost");

        jtfPort.setText("8122");

        jButton2.setText("Configure");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel4.setText("Basepath:");

        jtfBasepath.setText("mpidsserver");

        jLabel8.setText("Download-Folder:");

        jtfDownloadFolder.setText("jTextField1");

        jLabel12.setText("Default generator String:");

        jLabel13.setText("Login Information:");

        jLabel14.setText("Username:");

        jLabel15.setText("Password:");

        jLabel16.setText("Hashed Password:");

        jlHashedPassword.setText(" ");

        jButton10.setText("Reset");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout
                .setHorizontalGroup(jPanel2Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                jPanel2Layout
                                        .createSequentialGroup()
                                        .addGap(24, 24, 24)
                                        .addGroup(
                                                jPanel2Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(
                                                                jPanel2Layout
                                                                        .createSequentialGroup()
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                        .addComponent(
                                                                                                jLabel8)
                                                                                        .addComponent(
                                                                                                jLabel1,
                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(
                                                                                                jLabel2,
                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(
                                                                                                jLabel4,
                                                                                                javax.swing.GroupLayout.Alignment.LEADING))
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addGroup(
                                                                                                jPanel2Layout
                                                                                                        .createSequentialGroup()
                                                                                                        .addComponent(
                                                                                                                jtfDownloadFolder,
                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                552,
                                                                                                                Short.MAX_VALUE)
                                                                                                        .addGap(96,
                                                                                                                96,
                                                                                                                96))
                                                                                        .addComponent(
                                                                                                jtfHostname,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                648,
                                                                                                Short.MAX_VALUE)
                                                                                        .addComponent(
                                                                                                jtfBasepath,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                648,
                                                                                                Short.MAX_VALUE)
                                                                                        .addComponent(
                                                                                                jtfPort,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                648,
                                                                                                Short.MAX_VALUE)))
                                                        .addGroup(
                                                                jPanel2Layout
                                                                        .createSequentialGroup()
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addGroup(
                                                                                                jPanel2Layout
                                                                                                        .createParallelGroup(
                                                                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                                        .addComponent(
                                                                                                                jLabel12)
                                                                                                        .addComponent(
                                                                                                                jLabel16)
                                                                                                        .addComponent(
                                                                                                                jLabel15))
                                                                                        .addComponent(
                                                                                                jLabel13)
                                                                                        .addComponent(
                                                                                                jLabel14,
                                                                                                javax.swing.GroupLayout.Alignment.TRAILING))
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addGroup(
                                                                                                jPanel2Layout
                                                                                                        .createSequentialGroup()
                                                                                                        .addGroup(
                                                                                                                jPanel2Layout
                                                                                                                        .createParallelGroup(
                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                                        .addComponent(
                                                                                                                                jtfGenerator,
                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                498,
                                                                                                                                Short.MAX_VALUE)
                                                                                                                        .addComponent(
                                                                                                                                jtfUsername,
                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                498,
                                                                                                                                Short.MAX_VALUE)
                                                                                                                        .addGroup(
                                                                                                                                jPanel2Layout
                                                                                                                                        .createSequentialGroup()
                                                                                                                                        .addComponent(
                                                                                                                                                jtfPassword,
                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                336,
                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                        .addPreferredGap(
                                                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                                                                        .addComponent(
                                                                                                                                                jButton10,
                                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                156,
                                                                                                                                                Short.MAX_VALUE)))
                                                                                                        .addPreferredGap(
                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                                        .addComponent(
                                                                                                                jButton2,
                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                90,
                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                        .addGroup(
                                                                                                jPanel2Layout
                                                                                                        .createSequentialGroup()
                                                                                                        .addComponent(
                                                                                                                jlHashedPassword)
                                                                                                        .addGap(0,
                                                                                                                590,
                                                                                                                Short.MAX_VALUE)))))
                                        .addContainerGap()));
        jPanel2Layout
                .setVerticalGroup(jPanel2Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                jPanel2Layout
                                        .createSequentialGroup()
                                        .addGap(19, 19, 19)
                                        .addGroup(
                                                jPanel2Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(
                                                                jPanel2Layout
                                                                        .createSequentialGroup()
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(
                                                                                                jLabel1)
                                                                                        .addComponent(
                                                                                                jtfHostname,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(
                                                                                                jLabel2)
                                                                                        .addComponent(
                                                                                                jtfPort,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(
                                                                                                jLabel4)
                                                                                        .addComponent(
                                                                                                jtfBasepath,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(
                                                                                                jLabel8)
                                                                                        .addComponent(
                                                                                                jtfDownloadFolder,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addGap(18, 18, 18)
                                                                        .addGroup(
                                                                                jPanel2Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(
                                                                                                jLabel12)
                                                                                        .addComponent(
                                                                                                jtfGenerator,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                        .addComponent(jLabel13)
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jLabel14))
                                                        .addComponent(
                                                                jtfUsername,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(
                                                jPanel2Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(
                                                                jPanel2Layout
                                                                        .createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                        .addComponent(jButton2)
                                                                        .addComponent(
                                                                                jtfPassword,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jButton10))
                                                        .addComponent(jLabel15))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(
                                                jPanel2Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel16)
                                                        .addComponent(jlHashedPassword))
                                        .addContainerGap(314, Short.MAX_VALUE)));

        jTabbedPane1.addTab("Settings", jPanel2);

        jLabel9.setText("Total number of requests on this machine:");

        jLabel10.setText("Total request time on this machine:");

        jLabel11.setText("Mean time for a request:");

        jtfTotalNumber.setEditable(false);
        jtfTotalNumber.setText("0");

        jtfTotalTime.setEditable(false);
        jtfTotalTime.setText("0");

        jtfMeanTime.setEditable(false);
        jtfMeanTime.setText("0");

        jButton4.setText("Update");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout
                .setHorizontalGroup(jPanel7Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                jPanel7Layout
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(
                                                jPanel7Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                false)
                                                        .addGroup(
                                                                jPanel7Layout
                                                                        .createSequentialGroup()
                                                                        .addComponent(jLabel9)
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(
                                                                                jtfTotalNumber,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                434,
                                                                                Short.MAX_VALUE))
                                                        .addGroup(
                                                                jPanel7Layout
                                                                        .createSequentialGroup()
                                                                        .addGroup(
                                                                                jPanel7Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(
                                                                                                jLabel10)
                                                                                        .addComponent(
                                                                                                jLabel11))
                                                                        .addGap(57, 57, 57)
                                                                        .addGroup(
                                                                                jPanel7Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(
                                                                                                jtfMeanTime)
                                                                                        .addComponent(
                                                                                                jtfTotalTime))))
                                        .addContainerGap(63, Short.MAX_VALUE))
                        .addGroup(
                                javax.swing.GroupLayout.Alignment.TRAILING,
                                jPanel7Layout.createSequentialGroup()
                                        .addContainerGap(724, Short.MAX_VALUE)
                                        .addComponent(jButton4).addContainerGap()));
        jPanel7Layout
                .setVerticalGroup(jPanel7Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                jPanel7Layout
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(
                                                jPanel7Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(
                                                                jPanel7Layout
                                                                        .createSequentialGroup()
                                                                        .addGap(39, 39, 39)
                                                                        .addComponent(
                                                                                jtfTotalTime,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(
                                                                jPanel7Layout
                                                                        .createSequentialGroup()
                                                                        .addGroup(
                                                                                jPanel7Layout
                                                                                        .createParallelGroup(
                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(
                                                                                                jLabel9)
                                                                                        .addComponent(
                                                                                                jtfTotalNumber,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jLabel10)))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(
                                                jPanel7Layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel11)
                                                        .addComponent(
                                                                jtfMeanTime,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton4).addContainerGap()));

        jTabbedPane1.addTab("Stats", jPanel7);

        jButton6.setText("Update");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(jPanel8Layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 822,
                        Short.MAX_VALUE)
                .addGroup(
                        jPanel8Layout
                                .createSequentialGroup()
                                .addGap(0, 691, Short.MAX_VALUE)
                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        131, javax.swing.GroupLayout.PREFERRED_SIZE)));
        jPanel8Layout.setVerticalGroup(jPanel8Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel8Layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 521,
                                Short.MAX_VALUE)));

        jTabbedPane1.addTab("Information", jPanel8);

        jlLogEntries.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };

            public int getSize() {
                return strings.length;
            }

            public Object getElementAt(int i) {
                return strings[i];
            }
        });
        jlLogEntries.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jlLogEntriesValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(jlLogEntries);

        jButton9.setText("Update");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(jPanel9Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel9Layout
                        .createSequentialGroup()
                        .addGroup(
                                jPanel9Layout
                                        .createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jScrollPane3,
                                                javax.swing.GroupLayout.DEFAULT_SIZE, 210,
                                                Short.MAX_VALUE)
                                        .addComponent(jButton9,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(logScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 606,
                                Short.MAX_VALUE)));
        jPanel9Layout.setVerticalGroup(jPanel9Layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                jPanel9Layout
                        .createSequentialGroup()
                        .addComponent(jButton9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(
                                jPanel9Layout
                                        .createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane3,
                                                javax.swing.GroupLayout.DEFAULT_SIZE, 533,
                                                Short.MAX_VALUE)
                                        .addComponent(logScrollPane,
                                                javax.swing.GroupLayout.DEFAULT_SIZE, 533,
                                                Short.MAX_VALUE))));

        jTabbedPane1.addTab("Communication", jPanel9);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addComponent(jTabbedPane1,
                javax.swing.GroupLayout.Alignment.TRAILING));
        layout.setVerticalGroup(layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                javax.swing.GroupLayout.Alignment.TRAILING,
                layout.createSequentialGroup().addContainerGap().addComponent(jTabbedPane1)
                        .addContainerGap()));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton2ActionPerformed
        savePreferences();
    }// GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
        queryDatabase(jTextField3.getText());
    }// GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton3ActionPerformed
        String recordList = Utilities.buildEntityURISegment(
                Integer.parseInt(jTextField5.getText()), Integer.parseInt(jTextField6.getText()));
        queryDatabase(recordList);

    }// GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton5ActionPerformed
        NewRecordDialog d = new NewRecordDialog(this, true);
        d.setGenerator(jtfGenerator.getText());
        d.setVisible(true);
    }// GEN-LAST:event_jButton5ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowClosing
        savePreferences();
    }// GEN-LAST:event_formWindowClosing

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton4ActionPerformed
        updateStats();
    }// GEN-LAST:event_jButton4ActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jTextField3ActionPerformed
        jButton1ActionPerformed(null);
    }// GEN-LAST:event_jTextField3ActionPerformed

    public final void displayInformation() {
        Info info = heartDBClient.getServerInfo();
        StringBuilder infoText = new StringBuilder();
        infoText.append("Database information:\n");
        infoText.append("------------------------------------------\n");
        infoText.append(String.format("Number of Records: %d%n" + "Number of RecordTypes: %d%n"
                + "Number of Properties: %d%n" + "Number of Files: %d%n", info.getnRecords(),
                info.getnRecordTypes(), info.getnProperties(), info.getnFiles()));

        infoText.append("\nDropOffBox-Information:\n");
        infoText.append("---------------------------------------------\n");
        infoText.append("Path: ").append(info.getDropOffBoxPath()).append("\nFiles:\n");
        for (String dobFilePath : info.getFlatFileList()) {
            infoText.append("  ").append(dobFilePath).append("\n");
        }
        jTextArea1.setText(infoText.toString());
        updateLog();
    }

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton6ActionPerformed
        displayInformation();
    }// GEN-LAST:event_jButton6ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton8ActionPerformed
        AddPropertyDialog d = new AddPropertyDialog(this, true);
        d.setGenerator(jtfGenerator.getText());
        d.setVisible(true);
    }// GEN-LAST:event_jButton8ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton7ActionPerformed
        NewRecordTypeDialog d = new NewRecordTypeDialog(this, true);
        d.setGenerator(jtfGenerator.getText());
        d.setVisible(true);
    }// GEN-LAST:event_jButton7ActionPerformed

    private void jlLogEntriesValueChanged(javax.swing.event.ListSelectionEvent evt) {// GEN-FIRST:event_jlLogEntriesValueChanged
        HTTPLogEntry entry = (HTTPLogEntry) jlLogEntries.getSelectedValue();
        if (entry != null) {
            jtaResp.setText("" + "==== URL ====\n" + entry.getAddress() + "\n\n"
                    + "==== SENT ====\n" + entry.getSendInfo() + "\n\n" + "==== RESPONSE: ====\n"
                    + entry.getRespInfo());
        }
    }// GEN-LAST:event_jlLogEntriesValueChanged

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton9ActionPerformed
        updateLog();
    }// GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton10ActionPerformed
        password = "";
        savePreferences();
    }// GEN-LAST:event_jButton10ActionPerformed

    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jTextField6ActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_jTextField6ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton11ActionPerformed
        try {
            DropOffBoxFilePickupDialog d = new DropOffBoxFilePickupDialog(this, true);
            d.setGenerator(jtfGenerator.getText());
            d.setVisible(true);
        } catch (InterruptedException e) {
            java.util.logging.Logger.getLogger(DropOffBoxFilePickupDialog.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, e);
        }
    }// GEN-LAST:event_jButton11ActionPerformed

    private void jbPostXMLActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jbPostXMLActionPerformed
        if (jFileChooser1.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                String[] options = new String[] { "Record", "RecordType", "Property", "File" };
                int iOpt = JOptionPane.showOptionDialog(this, "Choose post-type:", "Post-type",
                        WIDTH, WIDTH, null, options, table);
                String fileContents = FileUtils.readFileToString(jFileChooser1.getSelectedFile());
                heartDBClient.postXML(fileContents, options[iOpt]);
            } catch (IOException ex) {
                Logger.getLogger(DBFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }// GEN-LAST:event_jbPostXMLActionPerformed

    public void queryDatabase(String recordList) {
        String comboBoxType = jComboBox1.getSelectedItem().toString();

        if (comboBoxType.equals("Record")) {
            table.setRecords(heartDBClient.retrieveRecord(recordList));
        }
        if (comboBoxType.equals("RecordType")) {
            table.setRecordTypes(heartDBClient.retrieveRecordType(recordList));
        }
        if (comboBoxType.equals("Property")) {
            table.setPropertyList(heartDBClient.retrieveProperty(recordList));
        }
        if (comboBoxType.equals("File")) {
            table.setFileList(heartDBClient.retrieveFile(recordList));
        }
        updateLog();
    }

    /**
     * @param args
     *            the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        // <editor-fold defaultstate="collapsed"
        // desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase
         * /tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
                    .getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DBFrame.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DBFrame.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DBFrame.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DBFrame.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        }
        // </editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DBFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JButton jbPostXML;
    private javax.swing.JLabel jlHashedPassword;
    private javax.swing.JList jlLogEntries;
    private javax.swing.JTextField jtfBasepath;
    private javax.swing.JTextField jtfDownloadFolder;
    private javax.swing.JTextField jtfGenerator;
    private javax.swing.JTextField jtfHostname;
    private javax.swing.JTextField jtfMeanTime;
    private javax.swing.JPasswordField jtfPassword;
    private javax.swing.JTextField jtfPort;
    private javax.swing.JTextField jtfTotalNumber;
    private javax.swing.JTextField jtfTotalTime;
    private javax.swing.JTextField jtfUsername;
    private javax.swing.JScrollPane logScrollPane;
    // End of variables declaration//GEN-END:variables
}
