import caosdb as db
import unittest
from caosdb.apiutils import new_record
from random import randint
from caosdb.apiutils import CaosDBPythonFile, CaosDBPythonRecord, convert_to_entity
import time



starttime = time.time()

general_container = None
logging = False
# If something went wrong during developing this file, 
# set the following to allow clean up before hand.
emergency_cleanup = False

def log(entry):
    if not logging:
        return 
    with open("log.txt", "a") as logfile:
        logfile.write(str(time.time()-starttime)+": ")
        logfile.write(str(entry))
        logfile.write("\n")

def setUpModule():
    log("Setting up test module")
    if emergency_cleanup:
        for q in ["FIND RECORD RT1 WITH A", "FIND RT1","FIND A", "FIND B", "FIND UselessFile"]:
            try:
                log(db.Query(q).execute().delete())
            except:
                pass

    a = db.Property(name = "A", datatype="text")
    log(a)
    b = db.Property(name = "B", datatype="text")
    log(b)
    f = db.RecordType(name = "UselessFile")
    log(f)
    rt = db.RecordType(name = "RT1").add_property(a).add_property(b).add_property(f)
    log(rt)
    global general_container
    general_container = db.Container()
    general_container.extend([a, b, f, rt]).insert()
    log(general_container)
    log("done setting up module")


def tearDownModule():
    log("tear down testing module")
    log(general_container.delete())
    log("done tearing down testing module")


class RecordTypeTemplateTestCase(unittest.TestCase):
    def tearDown(self):
        try:
            log(self.rec.delete())
        except:
            pass
        log("done")

        
class NewRecordTestCase(RecordTypeTemplateTestCase):
        
    def test_creation(self):
        log( "test_creation")
        self.rec = new_record("RT1", description="RWW" + str(randint(1, 9999999)), insert=True,
           A="bla" + str(randint(1, 9999999)), B=12)
    def test_creation_without_insert(self):
        log( "test_creation_without_insert")
        self.rec = new_record("RT1", description="RWW" + str(randint(1, 9999999)), insert=False,
           A="bla" + str(randint(1, 9999999)), B=12)
        self.rec.insert()


class PyCaosDbTestCase(RecordTypeTemplateTestCase):

    def test_insertion(self):
        log("test_insertion")
        f = CaosDBPythonFile()
        f.file = __file__
        f.path = "pyCaosDbTestcase/file"
        f.add_parent(parent_name = "UselessFile")
        r = CaosDBPythonRecord()
        r.add_parent(parent_name = "RT1")
        r.add_property(name = "UselessFile", value = f)
        r.add_property(name = "A", value = "hiA")
        r.add_property(name = "B", value = "hiB")
        e = convert_to_entity(r, recursive=True)
        log(e)
        self.rec = db.Container()
        self.rec.extend(e)
        self.rec.insert()

class PyCaosDbTestCaseWithFile(RecordTypeTemplateTestCase):
    def setUp(self):
        super(PyCaosDbTestCaseWithFile, self).setUp()
        #create file in db
        f = CaosDBPythonFile()
        f.file = __file__
        f.path = "test_apiutils/test_reference_existing"
        f.add_parent("UselessFile")
        self.dbf = db.Container().extend(convert_to_entity(f))
        self.dbf.insert()
        self.id_ = self.dbf[0].id

    def tearDown(self):
        super(PyCaosDbTestCaseWithFile, self).tearDown()
        self.dbf.delete()

    def test_reference_existing(self):
        log("test_reference_existing")
        r = CaosDBPythonRecord()
        r.add_parent(parent_name = "RT1")
        r.add_property(name = "UselessFile", value = self.id_)
        e = convert_to_entity(r, recursive=True)
        log(e)
        self.rec = db.Container()
        self.rec.extend(e)
        self.rec.insert()


if __name__ == '__main__':
    unittest.main(verbosity=2)
