/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
#include "heartdb.h"

int main()
{

	heartdb::configureConnection("http://localhost:8101/mpidsserver");

	/*recordType* rt= new recordType();
	rt->setName("TESTESTEST");
	rt->setDescription("simpleTest");
	rt->setCuid(1337);
	concreteProperty* cP = new concreteProperty();
	cP->setId(199);
	cP->setImportance("suggested");
	cP->setValue("15");
	rt->setProperty(cP);

	parent *par = new parent();
	par->setId(189);
	par->setTransfer("all");
	rt->setParent(par);

	recordType* rt2= new recordType();
	rt2->setName("TE3STESTEST");
	rt2->setDescription("simpleTest");
	rt2->setCuid(1338);
	rt2->setParent(par);
	list<recordType*> tmp;
	tmp.push_back(rt);
	tmp.push_back(rt2);
	heartdb::insertRecordTypes(tmp);*/

	


	
	/*file* tmp = new file();
	//tmp->setId(221);
	//tmp->setFile("tmp");
	tmp->setPath("b3430");
	tmp->setName("test");
	tmp->setPickup("test");
	/*file* tmp2 = new file();
	//tmp->setId(221);
	tmp2->setFile("tmp2");
	tmp2->setPath("b40");
	tmp2->setName("test");
	list<file*> fileList;
	fileList.push_back(tmp);
	heartdb::insertFile(fileList, DROP_OFF_BOX);*/
	
	list<file*> fileList=heartdb::retriveFile("351", HTTP_DOWNLOAD);
	fileList.front()->setFile("tmp");
	heartdb::updateFileRepresentations(fileList,HTTP_UPLOAD);
	/*list<file*> tmp;
	tmp = heartdb::retriveAllFileRepesentations();
	heartdb::downloadFiles(tmp, HTTP_DOWNLOAD);*/
	
	return 0;
}
