/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* 
 * File:   heartdb.cpp
 * Author: chris-andre
 */
#include "heartdb.h"

/**
Singleton
**/
heartdb* heartdb::instance = 0;
	
heartdb& heartdb::getInstance(){
	if(instance==0)	{
		static heartdb tmp;
		instance = &tmp;
	}
	return *instance;
}

bool heartdb::isInstantiated(){
	if (instance==0)
		return false;
	return true;
}

bool heartdb::isConfigured(){
	if (instance==0)
		return false;
	else{
		if(instance->getUrl()=="")
			return false;
	}
	return true;
}
	
void heartdb::configureConnection(string url){
	heartdb::configureConnection(url, 0, "", "", "");
}

void heartdb::configureConnection(string url, string path){
	heartdb::configureConnection(url, 0, path, "", "");
}
	
void heartdb::configureConnection(string url, string user, string password){
	heartdb::configureConnection(url, 0, "", user, password);
	
}

void heartdb::configureConnection(string url, string path, string user, string password){
	heartdb::configureConnection(url, 0, path, user, password);
}

void heartdb::configureConnection(string domain, int port){
	heartdb::configureConnection(domain, port, "", "", "");
}

void heartdb::configureConnection(string domain, int port, string path){
	heartdb::configureConnection(domain, port, path, "", "");
}

void heartdb::configureConnection(string domain, int port, string user, string password){
	heartdb::configureConnection(domain, port, "", user, password);
}

void heartdb::configureConnection(string domain, int port, string path, string user, string password){
	heartdb::getInstance();
	instance->setUrl(domain);
	instance->setPort(port);
	instance->setUser(user);
	instance->setPassword(password);
	if(path!="")
		instance->setPath("/"+path);	
}

string heartdb::buildUrl(){
	stringstream ss;
	if(this->getPort()){
		ss << this->getPort();
		return this->getUrl()+":"+ss.str()+path;	
	}
	return this->getUrl()+path;
}

/**
Utility function for curl that writes incoming data from the buffer into a string
**/

size_t heartdb::WriteCallback(void *buffer, size_t size, size_t nmemb, void *userp){
	((string*)userp)->append((char*)buffer, size * nmemb);
	return size * nmemb;
}

string heartdb::pull(string id, string type){
	CURL* curl;
	CURLcode res;
	string stream, url;
	struct curl_slist *headerlist=NULL;
	if(isConfigured())
		url = instance->buildUrl()+"/"+type+"/"+id;
	else{
		cout << "Please call heartdb::configureConnection with connection parameters first" << endl;
		return "";
		}
	string auth="Authorization:"+instance->getUser()+":"+instance->getPassword();
	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	//headerlist = curl_slist_append(headerlist, auth.c_str());
	//curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream);
	res = curl_easy_perform(curl);
	if (res != CURLE_OK){
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    }
	curl_slist_free_all(headerlist);
	curl_easy_cleanup(curl);
	return stream;
}

string heartdb::push(string type, string xml){
	CURL* curl;
	CURLcode res;
	string stream, url;
	struct curl_slist *headerlist=NULL;
	if(isConfigured())
		url = instance->buildUrl()+"/"+type;
	else{
		cout << "Please call heartdb::configureConnection with connection parameters first" << endl;
		return "";
	}
	string auth="Authorization:"+instance->getUser()+":"+instance->getPassword();
	curl = curl_easy_init();
	//POST
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_POST, true);
	headerlist = curl_slist_append(headerlist, " Content-Type: text/xml; charset=utf-8");
	//headerlist = curl_slist_append(headerlist, auth.c_str());
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, xml.c_str());
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, xml.length());
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
	//RESPONSE
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream);
	res = curl_easy_perform(curl);
	if(res != CURLE_OK)
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	curl_easy_cleanup(curl);
	curl_slist_free_all(headerlist);
	return stream;
}

string heartdb::push(list<file*>& fileList){
	CURL* curl;
	CURLcode res;
	string stream, url;
	struct curl_httppost* post = NULL;
 	struct curl_httppost* last = NULL;
	struct curl_slist *headerlist=NULL;
	if(isConfigured())
		url = instance->buildUrl()+"/"+"File";
	else{
		cout << "Please call heartdb::configureConnection with connection parameters first" << endl;
		return "";
	}
	curl_global_init(CURL_GLOBAL_ALL);
	curl_formadd(&post, &last,
        	CURLFORM_COPYNAME, "FileRepresentation",
        	CURLFORM_COPYCONTENTS, file::toXml(fileList).c_str(),
        	CURLFORM_END);
	for (list<file*>::iterator it = fileList.begin(); it != fileList.end(); it++){	
		file* f = (*it);
		curl_formadd(&post, &last,
              		CURLFORM_COPYNAME, "file",
               		CURLFORM_BUFFER, f->getChecksum().c_str(),
               		CURLFORM_BUFFERPTR, f->getFile(),
               		CURLFORM_BUFFERLENGTH, f->getSize(),
               		CURLFORM_END);
	}
	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
	headerlist = curl_slist_append(headerlist, " Content-Type: text/xml; charset=utf-8");
 	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
	//RESPONSE
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream);
	res = curl_easy_perform(curl);
	if(res != CURLE_OK)
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	curl_easy_cleanup(curl);
	curl_formfree(post);
	curl_slist_free_all(headerlist);
	return stream;
}

void heartdb::update(list<file*>& fileList){
	CURL* curl;
	CURLcode res;
	string stream, url;
	struct curl_httppost* post = NULL;
 	struct curl_httppost* last = NULL;
	struct curl_slist *headerlist=NULL;
	if(isConfigured())
		url = instance->buildUrl()+"/"+"File";
	else{
		cout << "Please call heartdb::configureConnection with connection parameters first" << endl;
		return ;
	}
	curl_global_init(CURL_GLOBAL_ALL);
	curl_formadd(&post, &last,
        	CURLFORM_COPYNAME, "FileRepresentation",
        	CURLFORM_COPYCONTENTS, file::toXml(fileList).c_str(),
        	CURLFORM_END);
	for (list<file*>::iterator it = fileList.begin(); it != fileList.end(); it++){	
		file* f = (*it);
		curl_formadd(&post, &last,
              		CURLFORM_COPYNAME, "file",
               		CURLFORM_BUFFER, f->getChecksum().c_str(),
               		CURLFORM_BUFFERPTR, f->getFile(),
               		CURLFORM_BUFFERLENGTH, f->getSize(),
               		CURLFORM_END);
	}
	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
	headerlist = curl_slist_append(headerlist, " Content-Type: text/xml; charset=utf-8");
 	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
	//RESPONSE
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream);
	res = curl_easy_perform(curl);
	if(res != CURLE_OK){
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		return;
	}
	curl_easy_cleanup(curl);
	curl_formfree(post);
	curl_slist_free_all(headerlist);
	cout << stream << endl;
	error::errorCheck(stream);
}

void heartdb::update(string type, string xml){
	CURL* curl;
	CURLcode res;
	string stream, url;
	struct curl_slist *headerlist=NULL;
	if(isConfigured())
		url = instance->buildUrl()+"/"+type;
	else{
		cout << "Please call heartdb::configureConnection with connection parameters first" << endl;
		return;
	}
	string auth="Authorization:"+instance->getUser()+":"+instance->getPassword();
	curl = curl_easy_init();
	//PUT
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	headerlist = curl_slist_append(headerlist, " Content-Type: text/xml; charset=utf-8");
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
	//headerlist = curl_slist_append(headerlist, auth.c_str());
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, xml.c_str());
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, xml.length());
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
	//RESPONSE
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream);
	res = curl_easy_perform(curl);
	if(res != CURLE_OK)
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	curl_easy_cleanup(curl);
	curl_slist_free_all(headerlist);
	error::errorCheck(stream);
}

size_t writeFile(char *ptr, size_t size, size_t nmemb, vector<char>* stream) {
	for(int i=0; i<nmemb*size; i++){
		stream->push_back(ptr[i]);
	}
	return nmemb*size;
}

void heartdb::download(list<file*>& fileList){
	CURL* curl;
	CURLcode res;
	string url;
	struct curl_slist *headerlist=NULL;
	if(!isConfigured()){
		cout << "Please call heartdb::configureConnection with connection parameters first" << endl;
		return;
	}
	curl = curl_easy_init();
	for (list<file*>::iterator it = fileList.begin(); it != fileList.end(); it++){	
		url = instance->buildUrl()+"/"+"FileSystem/"+(*it)->getPath();
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeFile);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &(*it)->fileBuffer);
		res = curl_easy_perform(curl);
		if(res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));	
		if(!(*it)->checkFile())
			it--;
	
	}	
	curl_easy_cleanup(curl);
	curl_slist_free_all(headerlist);
}

void heartdb::del(string id, string type){
	CURL* curl;
	CURLcode res;
	string stream, url;
	struct curl_slist *headerlist=NULL;
	if(isConfigured())
		url = instance->buildUrl()+"/"+type+"/"+id;
	else{
		cout << "Please call heartdb::configureConnection with connection parameters first" << endl;
		}
	string auth="Authorization:"+instance->getUser()+":"+instance->getPassword();
	curl = curl_easy_init();
	//Delete
	//headerlist = curl_slist_append(headerlist, auth.c_str());
	//curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl,CURLOPT_CUSTOMREQUEST,"DELETE");
	//RESPONSE
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream);
	res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	curl_slist_free_all(headerlist);
	curl_easy_cleanup(curl);
	error::errorCheck(stream);
}

list<abstractProperty*> heartdb::retrieveAllAbstractProperties(){
	return abstractProperty::fromXml(heartdb::pull("all", "Property"));
}

list<abstractProperty*> heartdb::retrieveAbstractProperty(int id){
	stringstream ss;
	ss << id;
	return heartdb::retrieveAbstractProperties(ss.str());

}

list<abstractProperty*> heartdb::retrieveAbstractProperties(int count, ...){
	va_list params; 
	stringstream ss;
	va_start(params, count);
	for (int i=0; i<count-1; i++)
    {
		ss << va_arg(params, int);
		ss << '&';
    }
	ss << va_arg(params, int);
	va_end(params);
	return heartdb::retrieveAbstractProperties(ss.str());
}

list<abstractProperty*> heartdb::retrieveAbstractProperty(string id){
	return abstractProperty::fromXml(heartdb::pull(id, "Property"));
}

list<abstractProperty*> heartdb::retrieveAbstractProperties(string id){
	return abstractProperty::fromXml(heartdb::pull(id, "Property"));
}

void heartdb::insertAbstractProperty(abstractProperty* aProp){
	list<abstractProperty*> aPropList;
	aPropList.push_back(aProp);
	heartdb::insertAbstractProperties(aPropList);
}

void heartdb::insertAbstractProperties(list<abstractProperty*> aPropList){
	string xmlPost = abstractProperty::toXml(aPropList);
	string xmlResponse = heartdb::push("Property", xmlPost);
	abstractProperty::setIdFromResponse(aPropList, xmlResponse);
}

void heartdb::updateAbstractProperty(abstractProperty* aProp){
	list<abstractProperty*> aPropList;
	aPropList.push_back(aProp);
	heartdb::updateAbstractProperties(aPropList);
}

void heartdb::updateAbstractProperties(list<abstractProperty*> aPropList){
	string xmlPost = abstractProperty::toXml(aPropList);
	heartdb::update("Property", xmlPost);
}

void heartdb::deleteAbstractProperties(string id){
     heartdb::del(id, "Property");
}

void heartdb::deleteAbstractProperty(int id){
    stringstream ss;
    ss << id;
    heartdb::del(ss.str(), "Property");
}

void  heartdb::deleteAbstractProperty(string id){
    heartdb::del(id, "Property");
}

void heartdb::deleteAbstractProperties(int count, ...){
    va_list params;
    stringstream ss;
    va_start(params, count);
    for (int i=0; i<count-1; i++)
    {
        ss << va_arg(params, int);
        ss << '&';
    }
    ss << va_arg(params, int);
    va_end(params);
    heartdb::del(ss.str(), "Property");
}

list<recordType*> heartdb::retrieveAllRecordTypes(){
	return recordType::fromXml(heartdb::pull("all", "RecordType"));
}

list<recordType*> heartdb::retrieveRecordType(string id){
	return recordType::fromXml(heartdb::pull(id, "RecordType"));
}

list<recordType*> heartdb::retrieveRecordType(int id){
	stringstream ss;
	ss << id;
	return recordType::fromXml(heartdb::pull(ss.str(), "RecordType"));
}

list<recordType*> heartdb::retrieveRecordTypes(string id){
	return recordType::fromXml(heartdb::pull(id, "RecordType"));
}

list<recordType*> heartdb::retrieveRecordTypes(int count, ...){
	va_list params; 
	stringstream ss;
	va_start(params, count);
	for (int i=0; i<count-1; i++)
    {
		ss << va_arg(params, int);
		ss << '&';
    }
	ss << va_arg(params, int);
	va_end(params);
	return recordType::fromXml(heartdb::pull(ss.str(), "RecordType"));
}

void heartdb::insertRecordType(recordType* rt){
	list<recordType*> recordTypeList;
	recordTypeList.push_back(rt);
	heartdb::insertRecordTypes(recordTypeList);
}

void heartdb::insertRecordTypes(list<recordType*> recordTypeList){
	string xmlPost = recordType::toXml(recordTypeList, "RecordType");
	string xmlResponse = heartdb::push("RecordType", xmlPost);
	recordType::setFromResponse(recordTypeList, xmlResponse);
}

void heartdb::updateRecordType(recordType* rt){
	list<recordType*> recordTypeList;
	recordTypeList.push_back(rt);
	heartdb::updateRecordTypes(recordTypeList);
}

void heartdb::updateRecordTypes(list<recordType*> recordTypeList){
	string xmlPost = recordType::toXml(recordTypeList, "RecordType");
	heartdb::update("RecordType", xmlPost);
}


void heartdb::deleteRecordType(string id){
	 heartdb::del(id, "RecordType");
}

void heartdb::deleteRecordType(int id){
	stringstream ss;
	ss << id;
	heartdb::del(ss.str(), "RecordType");
}

void  heartdb::deleteRecordTypes(string id){
	heartdb::del(id, "RecordType");
}

void heartdb::deleteRecordTypes(int count, ...){
	va_list params; 
	stringstream ss;
	va_start(params, count);
	for (int i=0; i<count-1; i++)
    {
		ss << va_arg(params, int);
		ss << '&';
    }
	ss << va_arg(params, int);
	va_end(params);
	heartdb::del(ss.str(), "RecordType");
}	

list<record*> heartdb::retrieveAllRecords(){
	list<recordType*> recordTypeList = recordType::fromXml(heartdb::pull("all", "Record"));
	list<record*> recordList;
	for (list<recordType*>::iterator it = recordTypeList.begin(), end = recordTypeList.end(); it != end; ++it){
		recordList.push_back((record*)*it);
	}
	return recordList;
}

list<record*> heartdb::retrieveRecord(string id){
	list<recordType*> recordTypeList = recordType::fromXml(heartdb::pull(id, "Record"));
	list<record*> recordList;
	for (list<recordType*>::iterator it = recordTypeList.begin(), end = recordTypeList.end(); it != end; ++it){
		recordList.push_back((record*)*it);
	}
	return recordList;
}

list<record*> heartdb::retrieveRecord(int id){
	stringstream ss;
	ss << id;
	list<recordType*> recordTypeList = recordType::fromXml(heartdb::pull(ss.str(), "Record"));
	list<record*> recordList;
	for (list<recordType*>::iterator it = recordTypeList.begin(), end = recordTypeList.end(); it != end; ++it){
		recordList.push_back((record*)*it);
	}
	return recordList;
}

list<record*> heartdb::retrieveRecords(string id){
	list<recordType*> recordTypeList = recordType::fromXml(heartdb::pull(id, "Record"));
	list<record*> recordList;
	for (list<recordType*>::iterator it = recordTypeList.begin(), end = recordTypeList.end(); it != end; ++it){
		recordList.push_back((record*)*it);
	}
	return recordList;
}

list<record*> heartdb::retrieveRecords(int count, ...){
	va_list params; 
	stringstream ss;
	va_start(params, count);
	for (int i=0; i<count-1; i++)
    {
		ss << va_arg(params, int);
		ss << '&';
    }
	ss << va_arg(params, int);
	va_end(params);
	list<recordType*> recordTypeList = recordType::fromXml(heartdb::pull(ss.str(), "Record"));
	list<record*> recordList;
	for (list<recordType*>::iterator it = recordTypeList.begin(), end = recordTypeList.end(); it != end; ++it){
		recordList.push_back((record*)*it);
	}
	return recordList;
}

void heartdb::insertRecord(record* r){
	list<record*> recordList;
	recordList.push_back(r);
	heartdb::insertRecords(recordList);
}

void heartdb::insertRecords(list<record*> recordList){
	list<recordType*> recordTypeList;
	for (list<record*>::iterator it = recordList.begin(), end = recordList.end(); it != end; ++it){
		recordTypeList.push_back((recordType*)*it);
	}
	string xmlPost = recordType::toXml(recordTypeList, "Record");
	string xmlResponse = heartdb::push("Record", xmlPost);
	recordType::setFromResponse(recordTypeList, xmlResponse);
}

void heartdb::updateRecord(record* r){
	list<record*> recordList;
	recordList.push_back(r);
	heartdb::updateRecords(recordList);
}

void heartdb::updateRecords(list<record*> recordList){
	list<recordType*> recordTypeList;
	for (list<record*>::iterator it = recordList.begin(), end = recordList.end(); it != end; ++it){
		recordTypeList.push_back((recordType*)*it);
	}
	string xmlPost = recordType::toXml(recordTypeList, "Record");
	heartdb::update("Record", xmlPost);
}

void heartdb::deleteRecord(string id){
	heartdb::del(id, "Record");
}

void heartdb::deleteRecord(int id){
	stringstream ss;
	ss << id;
	heartdb::del(ss.str(), "Record");
}

void heartdb::deleteRecords(string id){
	heartdb::del(id, "Record");
}

void heartdb::deleteRecords(int count, ...){
	va_list params; 
	stringstream ss;
	va_start(params, count);
	for (int i=0; i<count-1; i++)
    {
		ss << va_arg(params, int);
		ss << '&';
    }
	ss << va_arg(params, int);
	va_end(params);
	heartdb::del(ss.str(), "Record");
}

list<file*> heartdb::retriveAllFileRepesentations(){
	return file::fromXml(heartdb::pull("all", "File"));
}

list<file*> heartdb::retriveFileRepesentation(string id){
	return file::fromXml(heartdb::pull(id, "File"));
}

list<file*> heartdb::retriveFileRepesentation(int id){
	stringstream ss;
	ss << id;
	return file::fromXml(heartdb::pull(ss.str(), "File"));
}

list<file*> heartdb::retriveFileRepesentations(string id){
	return file::fromXml(heartdb::pull(id, "File"));
}

list<file*> heartdb::retriveFileRepesentations(int count, ...){
	va_list params; 
	stringstream ss;
	va_start(params, count);
	for (int i=0; i<count-1; i++)
    {
		ss << va_arg(params, int);
		ss << '&';
    }
	ss << va_arg(params, int);
	va_end(params);
	return file::fromXml(heartdb::pull(ss.str(), "File"));
}

list<file*> heartdb::retriveFile(string id, int flag){
	list<file*> fileList= file::fromXml(heartdb::pull(id, "File"));
	heartdb::downloadFiles(fileList, flag);
	return fileList;
}

list<file*> heartdb::retriveFile(int id, int flag){
	stringstream ss;
	ss << id;
	list<file*> fileList= file::fromXml(heartdb::pull(ss.str(), "File"));
	heartdb::downloadFiles(fileList, flag);
	return fileList;
}

list<file*> heartdb::retriveFileRepesentations(string id, int flag){
	list<file*> fileList= file::fromXml(heartdb::pull(id, "File"));
	heartdb::downloadFiles(fileList, flag);
	return fileList;
}

list<file*> heartdb::retrieveAllFiles(int flag){
	list<file*> fileList= file::fromXml(heartdb::pull("all", "File"));
	heartdb::downloadFiles(fileList, flag);
	return fileList;
}

void heartdb::downloadFile(file* f, int flag){
	list<file*> fileList;
	fileList.push_back(f);
	heartdb::downloadFiles(fileList, flag);
}

void heartdb::downloadFiles(list<file*>& fileList, int flag){
	heartdb::download(fileList);
}

void heartdb::insertFile(file* f, int flag){
	list<file*> fileList;
	fileList.push_back(f);
	heartdb::insertFile(fileList, flag);
}

void heartdb::insertFile(list<file*>& fileList, int flag){
	if(flag==HTTP_UPLOAD){
		string xmlResponse = heartdb::push(fileList);
		file::setFromResponse(fileList, xmlResponse);
	}
	else{
		string xmlPost = file::toXml(fileList);
		string xmlResponse = heartdb::push("FilesDropOff", xmlPost);
		file::setFromResponse(fileList, xmlResponse);
	}
}

void heartdb::updateFileRepresentation(file* f, int flag){
	list<file*> fileList;
	fileList.push_back(f);
	heartdb::updateFileRepresentations(fileList, flag);
}

void heartdb::updateFileRepresentations(list<file*>& fileList, int flag){
	if(flag==HTTP_UPLOAD){	
		heartdb::update(fileList);
	}
	else{
		string xmlPost = file::toXml(fileList);
		heartdb::update("FilesDropOff", xmlPost);
	}
}

void heartdb::deleteFileRepresentation(string id){
	heartdb::del(id, "File");
}

void heartdb::deleteFileRepresentation(int id){
	stringstream ss;
	ss << id;
	heartdb::del(ss.str(), "File");
}

void heartdb::deleteFileRepresentations(string id){
	heartdb::del(id, "File");
}

void heartdb::deleteFileRepresentations(int count, ...){
	va_list params; 
	stringstream ss;
	va_start(params, count);
	for (int i=0; i<count-1; i++)
    {
		ss << va_arg(params, int);
		ss << '&';
    }
	ss << va_arg(params, int);
	va_end(params);
	heartdb::del(ss.str(), "File");
}
