/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* 
 * File:   heartdb.h
 * Author: chris-andre
 */

#ifndef HEARTDB_H
#define	HEARTDB_H

#include <string>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <list>
#include "curl/curl.h"
#include "pugixml.hpp"
#include <sstream>
#include "property.h"
#include "records.h"
#include "file.h"
#include <cstdarg>
#include "error.h"

using namespace std;

class heartdb
{
	public:
		
		//Setter
		void setUrl(string url){this->url=url;}
		void setPort(int port){this->port=port;}
		void setPath(string path){this->path=path;}
		void setUser(string user){this->user=user;}
		void setPassword(string password){this->password=password;}

		//Getter
		string getUrl(){return url;}
		int getPort(){return port;}
		string getPath(){return path;}
		string getUser(){return user;}
		string getPassword(){return password;}
		
		//Connection
		static heartdb& getInstance();
		static void configureConnection(string url);
		static void configureConnection(string url, string path);
		static void configureConnection(string url, string user, string password);
		static void configureConnection(string url, string path, string user, string password);
		static void configureConnection(string domain, int port);
		static void configureConnection(string domain, int port, string path);
		static void configureConnection(string domain, int port, string user, string password);
		static void configureConnection(string domain, int port, string path, string user, string password);
		static bool isInstantiated();
		static bool isConfigured();
		
		//retrieve Abstract Property(ies) from heartdb
		static list<abstractProperty*> retrieveAbstractProperties(string id);
		static list<abstractProperty*> retrieveAbstractProperties(int count, ... );
		static list<abstractProperty*> retrieveAbstractProperty(int id);
		static list<abstractProperty*> retrieveAbstractProperty(string id);
		
		//retrieve all Abstract Propertyies from heartdb
		static list<abstractProperty*> retrieveAllAbstractProperties();
		
		//insert AbstractProperty(ies) to heartdb
		static void insertAbstractProperty(abstractProperty* aProp);
		static void insertAbstractProperties(list<abstractProperty*> aPropList);

		//update AbstractProperty(ies) in heartdb
		static void updateAbstractProperty(abstractProperty* aProp);
		static void updateAbstractProperties(list<abstractProperty*> aPropList);

		//delete AbstractProperty(ies) from heartdb
		static void deleteAbstractProperties(string id);
		static void deleteAbstractProperty(string id);
		static void deleteAbstractProperty(int id);
		static void deleteAbstractProperties(int count, ...);
		
		//retrieve RecordType(s) from heartdb
		static list<recordType*> retrieveRecordTypes(string id);
		static list<recordType*> retrieveRecordType(string id);
		static list<recordType*> retrieveRecordType(int id);
		static list<recordType*> retrieveRecordTypes(int count, ...);
		
		//retrieve all Abstract RecordTypes from heartdb
		static list<recordType*> retrieveAllRecordTypes();
	
		//insert RecordType(s) to heartdb
		static void insertRecordType(recordType* rt);
		static void insertRecordTypes(list<recordType*> recordTypeList);
		
		//update RecordType(s) in heartdb
		static void updateRecordType(recordType* rt);
		static void updateRecordTypes(list<recordType*> recordTypeList);
		
		//delete RecordType(s) from heartdb
		static void deleteRecordTypes(string id);
		static void deleteRecordType(string id);
		static void deleteRecordType(int id);
		static void deleteRecordTypes(int count, ...);
		
		//retrieve Record(s) from heartdb
		static list<record*> retrieveRecords(string id);
		static list<record*> retrieveRecord(string id);
		static list<record*> retrieveRecord(int id);
		static list<record*> retrieveRecords(int count, ...);
		
		//retrieve all Abstract RecordTypes from heartdb
		static list<record*> retrieveAllRecords();
		
		//insert Record(s) to heartdb
		static void insertRecord(record* r);
		static void insertRecords(list<record*> recordList);

		//update Record(s) in heartdb
		static void updateRecord(record* r);
		static void updateRecords(list<record*> recordList);
		
		//delete Record(s) from heartdb
		static void deleteRecords(string id);
		static void deleteRecord(string id);
		static void deleteRecord(int id);
		static void deleteRecords(int count, ...);
		
		//retrieve FileRepresentation(s) from heartdb
		static list<file*> retriveFileRepesentation(string id);
		static list<file*> retriveFileRepesentations(string id);
		static list<file*> retriveFileRepesentation(int id);
		static list<file*> retriveFileRepesentations(int count, ...);

		//retrieve all FileRepresentations from heartdb
		static list<file*> retriveAllFileRepesentations();
		
		//retrieve all FileRepresentations and Files from heartdb
		static list<file*> retrieveAllFiles(int flag);
		
		//retrieve FileRepresentation(s) and File(s) from heartdb
		static list<file*> retriveFile(string id, int flag);
		static list<file*> retriveFile(int id, int flag);
		static list<file*> retriveFileRepesentations(string id, int flag);

		//insert File(s) to heartdb
		static void insertFile(file* f, int flag);
		static void insertFile(list<file*>& fileList, int flag);
		
		//download File(s) to buffer
		static void downloadFile(file* f, int flag);
		static void downloadFiles(list<file*>& fileList, int flag);

		//update FileRepresentation(s) in heartdb
		static void updateFileRepresentation(file* f, int flag);
		static void updateFileRepresentations(list<file*>& fileList, int flag);

		//delete FileRepresentation(s) from heartdb
		static void deleteFileRepresentation(string id);
		static void deleteFileRepresentations(string id);
		static void deleteFileRepresentation(int id);
		static void deleteFileRepresentations(int count, ...);

		
  	private:
		//Attribute
		static heartdb* instance;
		string url;
		int port;
		string path;
		string user;
		string password;
 
		//Private constructors
		heartdb() {}
		heartdb(const heartdb& orig);
		
		//private functions
		static size_t WriteCallback(void *buffer, size_t size, size_t nmemb, void *userp);
		static size_t WriteFile(void *buffer, size_t size, size_t nmemb, void *userp);
		static string pull(string id, string type);
		static string push(string type, string xml);
		static string push(list<file*>& fileList);
		static void update(string type, string xml);
		static void update(list<file*>& fileList);
		static void del(string id, string type);
		static void download(list<file*>& fileList);
		string buildUrl();
  
};

#endif	
