/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
#include "file.h"

void file::setFile(string name){
	FILE * pFile;
	long lSize;
	size_t result;
	unsigned char digest[SHA512_DIGEST_LENGTH];
	char* buffer;
	pFile = fopen (name.c_str() , "r");
   	if (pFile == NULL) 
		perror ("Error opening file");
	fseek (pFile , 0 , SEEK_END);
  	lSize = ftell (pFile);
	rewind (pFile);
	buffer= new char[lSize];
	result = fread (buffer,1,lSize,pFile);
	if (result != lSize) {
		fputs ("Reading error",stderr); exit (3);
	}
	SHA512_CTX ctx;
	SHA512_Init(&ctx);
    	SHA512_Update(&ctx, buffer, lSize);
    	SHA512_Final(digest, &ctx);
	char mdString[SHA512_DIGEST_LENGTH*2+1];
    	for (int i = 0; i < SHA512_DIGEST_LENGTH; i++)
        	sprintf(&mdString[i*2], "%02x", (unsigned int)digest[i]);
	this->setChecksum(mdString);
	this->setUpload(mdString);
	this->setSize(lSize);
	this->fileBuffer.assign (buffer,buffer+lSize);
	delete[] buffer;
}

bool file::checkFile(){
	unsigned char digest[SHA512_DIGEST_LENGTH];
	SHA512_CTX ctx;
	SHA512_Init(&ctx);
    	SHA512_Update(&ctx, this->getFile(), this->getSize());
    	SHA512_Final(digest, &ctx);
	char mdString[SHA512_DIGEST_LENGTH*2+1];
    	for (int i = 0; i < SHA512_DIGEST_LENGTH; i++)
        	sprintf(&mdString[i*2], "%02x", (unsigned int)digest[i]);
	for (int i = 0; i < 128; i++){
		if(!isdigit(mdString[i]))
			mdString[i]=toupper(mdString[i]);
	}

	if(this->getChecksum().compare(mdString)){
		cout << "ID: "<< this->getId() << " download failed. Retrying..." << endl;
		return false; 		
	}

	cout << "ID: "<< this->getId() << " successfully downloaded" << endl;	
	return true;
}

list<file*> file::fromXml(string stream){
	list<file*> fileList;
	stringstream ss;
	ss.str (stream);
	pugi::xml_document doc;
	pugi::xml_parse_result xmlres = doc.load(ss);
	file* f;
	for (pugi::xml_node node = doc.child("Response").first_child(); node; node = node.next_sibling()){
		if(error::errorCheck(node)){
			f = new file();
			string id= node.attribute("id").value();
			f->setId(atoi(id.c_str()));
			f->setName(node.attribute("name").value());
			f->setPickup(node.attribute("pickup").value());
			f->setDescription(node.attribute("description").value());
			f->setChecksum(node.attribute("checksum").value());
			string size= node.attribute("size").value();
			f->setSize(atoi(size.c_str()));
			f->setPath(node.attribute("path").value());		
			fileList.push_back(f);
		}			
	}
	return fileList;
}

string file::toXml(list<file*> fileList){
	pugi::xml_document doc;
	pugi::xml_node root = doc.append_child("Post");
	int i=0;
	for (list<file*>::iterator it = fileList.begin(); it != fileList.end(); it++){	
		pugi::xml_node file = root.append_child("File");
		if((*it)->getId()!=0)
			file.append_attribute("id") = (*it)->getId();
		if((*it)->getUpload()!="")
			file.append_attribute("upload") = (*it)->getUpload().c_str();
		if((*it)->getName()!="")
			file.append_attribute("name") = (*it)->getName().c_str();
		if((*it)->getDescription()!="")
			file.append_attribute("description") = (*it)->getDescription().c_str();
		if((*it)->getPath()!="")
			file.append_attribute("path") = (*it)->getPath().c_str();	
		if((*it)->getChecksum()!="")
			file.append_attribute("checksum") = (*it)->getChecksum().c_str();
		if((*it)->getSize()!=0)
			file.append_attribute("size") = (*it)->getSize();
		file.append_attribute("cuid") = i++;
		if((*it)->getPickup()!="")
			file.append_attribute("pickup") = (*it)->getPickup().c_str();;
	}
	stringstream stream;  
	doc.print(stream);
   	return stream.str();
}

void file::setFromResponse(list<file*>& fileList, string xmlResponse){
	stringstream ss;
	ss.str (xmlResponse);
	pugi::xml_document doc;
	pugi::xml_parse_result xmlres = doc.load(ss);
	for (pugi::xml_node node = doc.child("Response").first_child(); node; node = node.next_sibling()){
		if(error::errorCheck(node)){		
			string id= node.attribute("id").value();
			for (list<file*>::iterator it = fileList.begin(); it != fileList.end(); it++){
				if(!(*it)->getPath().compare(node.attribute("path").value())){	
					(*it)->setId(atoi(id.c_str()));
					(*it)->setChecksum(node.attribute("checksum").value());
					string size= node.attribute("size").value();
					(*it)->setSize(atoi(size.c_str()));
					cout << "File successfully inserted. ID: " << (*it)->getId() << endl;
					break;
				}
			}		
		}
	}
}
