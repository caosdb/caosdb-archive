/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
#include "property.h"

list<abstractProperty*>  abstractProperty::fromXml(string stream){
	list<abstractProperty*> aPropList;
	stringstream ss;
	ss.str (stream);
	pugi::xml_document doc;
	pugi::xml_parse_result xmlres = doc.load(ss);
	for (pugi::xml_node node = doc.child("Response").first_child(); node; node = node.next_sibling()){
		if(error::errorCheck(node)){			
			abstractProperty* aProp = new abstractProperty();
			string id= node.attribute("id").value();
			aProp->setId(atoi(id.c_str()));
			aProp->setName(node.attribute("name").value());
			aProp->setType(node.attribute("type").value());
			aProp->setDescription(node.attribute("description").value());
			aProp->setGenerator(node.attribute("generator").value());
			aProp->setUnit(node.attribute("unit").value());
			string reference= node.attribute("reference").value();
			aProp->setReference(atoi(reference.c_str()));
			aProp->setCreator(node.attribute("creator").value());
			aProp->setCreated(node.attribute("created").value());
			aPropList.push_back(aProp);
			}
		}
		return aPropList;
}

//list<Object> -> XML

string abstractProperty::toXml(list<abstractProperty*> aPropList){
	pugi::xml_document doc;
	pugi::xml_node root = doc.append_child("Post");
	for (list<abstractProperty*>::iterator aProp = aPropList.begin(), end = aPropList.end(); aProp != end; ++aProp)
	{
		pugi::xml_node property = root.append_child("Property");
		if((*aProp)->getId()!=0)
			property.append_attribute("id") = (*aProp)->getId();
		if((*aProp)->getName()!="")
			property.append_attribute("name") = (*aProp)->getName().c_str();
		if((*aProp)->getDescription()!="")
			property.append_attribute("description") = (*aProp)->getDescription().c_str();
		if((*aProp)->getGenerator()!="")
			property.append_attribute("generator") = (*aProp)->getGenerator().c_str();	
		if((*aProp)->getType()!="")
			property.append_attribute("type") = (*aProp)->getType().c_str();
		if((*aProp)->getUnit()!="")
			property.append_attribute("unit") = (*aProp)->getUnit().c_str();
		if((*aProp)->getReference()!=0)
			property.append_attribute("reference") = (*aProp)->getReference();
    }	
	stringstream stream;  
	doc.print(stream);
   	return stream.str();
}

void abstractProperty::setIdFromResponse(list<abstractProperty*> aPropList, string xmlResponse){
	stringstream ss;
	ss.str (xmlResponse);
	pugi::xml_document doc;
	pugi::xml_parse_result xmlres = doc.load(ss);
	
	list<abstractProperty*>::iterator aProp = aPropList.begin(), end = aPropList.end(); 
	
	for (pugi::xml_node node = doc.child("Response").first_child(); node && aProp != end; node = node.next_sibling(), ++aProp){
		if(error::errorCheck(node)){			
			string id= node.attribute("id").value();
			(*aProp)->setId(atoi(id.c_str()));
			}
		}
}
