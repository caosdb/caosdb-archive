/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-25
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ds.mpg.de.jheartdb.structures;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.ClientProtocolException;
import org.jdom2.DataConversionException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import ds.mpg.de.jheartdb.HeartDBClient;
import ds.mpg.de.jheartdb.utils.Utilities;

/**
 * HeartDB-Structure for files.
 * 
 * @author salexan
 */
public class File extends Entity {

    private java.io.File file;
    private String path;
    private String checksum;
    private Long size;
    private String upload;
    private String pickup;

    public File() {
    }

    public File(final java.io.File file, final String path) {
        this.file = file;
        this.path = path;
    }

    public File(final String pickup, final String path) {
        this.pickup = pickup;
        this.path = path;
    }

    public File(final Element e) {
        setFromElement(e);
    }

    @Override
    public Element createElement() {
        final Element e = createElement("File");
        if (path != null) {
            e.setAttribute("destination", path);
        }
        if (pickup != null) {
            e.setAttribute("pickup", pickup);
        }
        if (upload != null) {
            e.setAttribute("upload", upload);
        }
        if (checksum != null) {
            e.setAttribute("checksum", checksum);
        }
        if (size != -1) {
            e.setAttribute("size", Long.toString(size));
        }
        return e;
    }

    @Override
    public void setFromElement(final Element e) {
        if (e.getAttribute("path") != null) {
            setPath(e.getAttributeValue("path"));
        }
        if (e.getAttribute("pickup") != null) {
            setPickup(e.getAttributeValue("pickup"));
        }
        if (e.getAttribute("checksum") != null) {
            setChecksum(e.getAttributeValue("checksum"));
        }
        if (e.getAttribute("size") != null) {
            setSize(Long.parseLong(e.getAttributeValue("size")));
        }
        super.setFromElement(e);
    }

    /**
     * Compare the checksum of the local file with the checksum of the file in
     * the file system of HeartDB.
     * 
     * @return true, if the file in the file system has the same checksum as the
     *         file pointed to by this file representation.
     * @author Timm Fitschen
     */
    public boolean checksumComparison() {
        HeartDBClient con;
        try {
            con = HeartDBClient.getInstance();
            if (con.canConnect()) {
                final File f = con.retrieveFile(getId()).getFirst();
                if (f.getChecksum().equals(getChecksum())) {
                    return true;
                }
            }
        } catch (final SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     *            the path to set
     */
    public void setPath(final String path) {
        this.path = path;
    }

    /**
     * @return the checksum
     */
    public String getChecksum() {
        if (checksum == null && file != null) {
            checksum = Utilities.getChecksum(file);
        }
        return checksum;
    }

    /**
     * @param checksum
     *            the checksum to set
     */
    public void setChecksum(final String checksum) {
        this.checksum = checksum;
    }

    /**
     * @return the size
     */
    public Long getSize() {
        if (size == null && file != null) {
            size = Utilities.getSize(file);
        }
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final long size) {
        this.size = size;
    }

    /**
     * @return the pickup
     */
    public String getPickup() {
        return pickup;
    }

    /**
     * @param pickup
     *            the pickup to set
     */
    public void setPickup(final String pickup) {
        this.pickup = pickup;
    }

    /**
     * @return the file
     */
    public java.io.File getFile() {
        return file;
    }

    /**
     * @param file
     *            the file to set
     */
    public void setFile(final java.io.File file) {
        this.file = file;
    }

    public boolean cpToDropOffBox(final String dropOffBoxPath) {
        try {
            if (file != null) {
                if (file.getCanonicalPath().startsWith(dropOffBoxPath)) {

                    pickup = file.getCanonicalPath().substring(dropOffBoxPath.length(),
                            file.getCanonicalPath().length() - 1);
                    checksum = Utilities.getChecksum(file);
                    size = Utilities.getSize(file);
                    return true;
                }
                checksum = Utilities.getChecksum(file);
                size = Utilities.getSize(file);
                final java.io.File target = new java.io.File(dropOffBoxPath + "/" + file.getName()
                        + checksum);
                if (target.exists()) {
                    return false;
                }
                if (file.isFile()) {
                    FileUtils.copyFile(file, target);
                    pickup = target.getName();
                    file = null;
                    return true;
                }
                if (file.isDirectory()) {
                    FileUtils.copyDirectory(file, target);
                    pickup = target.getName();
                    file = null;
                    return true;
                }
            }
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    /**
     * The file upload via http requires an identification of files in the xml
     * which contains the meta-information about the files, and the
     * representation of the files in the other parts of the http entity. Thus,
     * we need a temporary identifier, conveniently generated from the filename
     * and the checksum. The upload attribute of the File tag, and the Field
     * name of the corresponding file are set to this temporary identifier.
     * 
     * This method has no further benefit for users of this client library and
     * is intended for the internal use.
     * 
     * @return temporary identifier
     * @author Timm Fitschen
     */
    public String getTempIdentifier() {
        return getFile().getName() + getChecksum();
    }

    /**
     * When the file entities are retrieved, the actual file is not
     * automatically retrieved. This method downloads it and stores it to a
     * temporary file.
     * 
     * @return true, iff everything went correctly
     * @author Timm Fitschen
     * @throws IOException
     * @throws URISyntaxException
     * @throws JDOMException
     * @throws IllegalStateException
     * @throws InterruptedException
     */
    public boolean download() throws IOException, URISyntaxException, IllegalStateException,
            JDOMException, InterruptedException {
        final String[] uriParts = path.split("/");
        final String tempName = uriParts[uriParts.length - 1]
                + Long.toHexString(System.currentTimeMillis());
        final java.io.File target = java.io.File.createTempFile(tempName, null);

        return download(target);
    }

    /**
     * When the file entities are retrieved, the actual file is not
     * automatically retrieved. This method downloads it and stores it to the
     * given target. Any problems will generate error messages in this file.
     * 
     * @param target
     * @return true, iff everything went correctly
     * @author Timm Fitschen
     * @throws URISyntaxException
     * @throws IOException
     * @throws ClientProtocolException
     * @throws JDOMException
     * @throws IllegalStateException
     * @throws InterruptedException
     */
    public boolean download(final java.io.File target) throws ClientProtocolException, IOException,
            URISyntaxException, IllegalStateException, JDOMException, InterruptedException {

        final Document doc = HeartDBClient.getInstance().downloadFile(this, target);
        if (doc == null) {
            return true;
        }
        final Element root = doc.getRootElement();
        for (final Element e : root.getChildren()) {
            try {
                getMessages().add(new Message(e));
            } catch (final DataConversionException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return false;
    }

}
