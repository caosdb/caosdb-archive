/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2012-02-15
 */

package ds.mpg.de.jheartdb.log;

import java.util.Date;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.jdom2.Document;

import ds.mpg.de.jheartdb.utils.Utilities;

/**
 * 
 * @author salexan
 */
public class HTTPLogEntry {

    private String comment = "NA";
    private long timestamp = -1L;
    private String uri = "NA";
    private String sendHeader = "NA";
    private String respHeader = "NA";
    private String sendBody = "NA";
    private String respBody = "NA";

    public HTTPLogEntry() {
        timestamp = new Date().getTime();
    }

    public HTTPLogEntry(final String comment, final String uri, final String sendHeader,
            final String respHeader, final String sendBody, final String respBody) {
        timestamp = new Date().getTime();
        if (comment != null) {
            this.comment = comment;
        }
        if (uri != null) {
            this.uri = uri;
        }
        if (sendHeader != null) {
            this.sendHeader = sendHeader;
        }
        if (respHeader != null) {
            this.respHeader = respHeader;
        }
        if (sendBody != null) {
            this.sendBody = sendBody;
        }
        if (respBody != null) {
            this.respBody = respBody;
        }
    }

    public HTTPLogEntry(final String comment, final String uri, final Header[] sendHeader,
            final Header[] respHeader, final HttpEntity sendBody, final HttpEntity respBody) {
        timestamp = new Date().getTime();
        if (comment != null) {
            this.comment = comment;
        }
        if (uri != null) {
            this.uri = uri;
        }
        if (sendBody != null) {
            this.sendBody = Utilities.Entity2String(sendBody);
        }
        if (respBody != null) {
            this.respBody = Utilities.Entity2String(respBody);
        }
        if (sendHeader != null) {
            final StringBuilder b = new StringBuilder();
            for (final Header h : sendHeader) {
                b.append(h.toString());
                b.append("\n");
            }
            this.sendHeader = b.toString();
        }
        if (respHeader != null) {
            final StringBuilder b = new StringBuilder();
            for (final Header h : respHeader) {
                b.append(h.toString());
                b.append("\n");
            }
            this.respHeader = b.toString();
        }
    }

    @Override
    public String toString() {
        return String.format("%1$tH:%1$tM (%2$s)", new Date(timestamp), comment);
    }

    public String getRespInfo() {
        final StringBuilder b = new StringBuilder();
        b.append("Header: ");
        b.append(respHeader);
        b.append("\n\n");
        b.append(respBody);
        return b.toString();
    }

    public String getSendInfo() {
        final StringBuilder b = new StringBuilder();
        b.append("Header: ");
        b.append(sendHeader);
        b.append("\n\n");
        b.append(sendBody);
        return b.toString();
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return uri;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(final String address) {
        uri = address;
    }

    // /**
    // * @return the response header
    // */
    // public String getSendHeader() {
    // return sendHeader;
    // }

    // /**
    // * @return the request header
    // */
    // public String getSendHeader() {
    // return respHeader;
    // }

    /**
     * @param header
     *            the response header to set
     */
    public void setRespHeader(final String header) {
        respHeader = header;
    }

    /**
     * @param headers
     *            the response headers to set
     */
    public void setRespHeader(final Header[] headers) {
        if (headers != null) {
            final StringBuilder b = new StringBuilder();
            for (final Header h : headers) {
                b.append(h.toString());
                b.append("\n");
            }
            respHeader = b.toString();
        }
    }

    /**
     * @param header
     *            the request header to set
     */
    public void setSendHeader(final String header) {
        sendHeader = header;
    }

    // /**
    // * @return the request body.
    // */
    // public String getSendBody() {
    // return sendBody;
    // }

    /**
     * @param body
     *            the request body to set
     */
    public void setSendBody(final String body) {
        sendBody = body;
    }

    // /**
    // * @return the response body
    // */
    // public String getRespBody() {
    // return respBody;
    // }

    /**
     * @param body
     *            the response body to set
     */
    public void setRespBody(final String body) {
        respBody = body;
    }

    public void setRespEntry(final Document doc) {
        setRespBody(Utilities.Document2String(doc));
    }

}
