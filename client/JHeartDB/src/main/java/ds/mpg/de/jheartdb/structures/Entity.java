/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-14
 * 
 * 2013-08-17: Timm Fitschen (timm.fitschen@ds.mpg.de)
 */
package ds.mpg.de.jheartdb.structures;

import java.util.HashMap;
import java.util.LinkedList;

import org.jdom2.DataConversionException;
import org.jdom2.Element;

import ds.mpg.de.jheartdb.structures.Message.MessageType;

/**
 * 
 * @author salexan, timm fitschen
 */
public abstract class Entity {
    private Integer id = null;
    private Integer cuid = null;
    private String name = null;
    private String description = null;
    private String generator = null;
    private String timestamp = null;
    private PropertyType type = null;
    private String unit = null;
    private Integer exponent = null;
    private Integer referenceTypeID = null;
    private String importance = null;
    private Object value = null;
    private LinkedList<Entity> parents = new LinkedList<Entity>();
    private LinkedList<Property> properties = new LinkedList<Property>();
    private LinkedList<Message> messages = new LinkedList<Message>();

    public enum Role {
        PROPERTY, RECORD, RECORDTYPE, FILE
    }

    public static Entity entityFactory(final Element e) throws DBException {
        final Role r = Role.valueOf(e.getName().toUpperCase());
        switch (r) {
        case PROPERTY:
            return new Property(e);
        case FILE:
            return new File(e);
        case RECORD:
            return new Record(e);
        case RECORDTYPE:
            return new RecordType(e);
        default:
            throw new DBException();
        }
    }

    public abstract Element createElement();

    protected Element createElement(final String name) {
        final Element e = new Element(name);
        if (getId() != null) {
            e.setAttribute("id", Integer.toString(getId()));
        }
        if (getCuid() != null) {
            e.setAttribute("cuid", Integer.toString(getCuid()));
        }
        if (getName() != null) {
            e.setAttribute("name", getName());
        }
        if (getDescription() != null) {
            e.setAttribute("description", getDescription());
        }
        if (getGenerator() != null) {
            e.setAttribute("generator", getGenerator());
        }
        if (getTimestamp() != null) {
            e.setAttribute("timestamp", getTimestamp());
        }
        if (getExponent() != null) {
            e.setAttribute("exponent", getExponent().toString());
        }
        for (final Entity p : getProperties()) {
            e.addContent(p.createElement("Property"));
        }
        for (final Entity p : getParents()) {
            e.addContent(p.createElement("Parent"));
        }
        for (final Message m : getMessages()) {
            e.addContent(m.createElement());
        }
        if (getType() != null) {
            e.setAttribute("type", getType().toString());
        }
        if (getUnit() != null) {
            e.setAttribute("unit", getUnit());
        }
        if (getReferenceTypeID() != null) {
            e.setAttribute("reference", Integer.toString(getReferenceTypeID()));
        }
        if (getValue() != null) {
            if (getValue() instanceof Entity) {
                e.setText(((Entity) getValue()).getId().toString());
            } else {
                e.setText(getValue().toString());
            }
        }
        if (getImportance() != null) {
            e.setAttribute("importance", getImportance());
        }
        return e;
    }

    public void setFromElement(final Element e) {
        if (e.getAttribute("cuid") != null) {
            try {
                setCuid(e.getAttribute("cuid").getIntValue());
            } catch (final DataConversionException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        if (e.getAttribute("name") != null) {
            setName(e.getAttributeValue("name"));
        }
        if (e.getAttribute("type") != null) {
            setType(e.getAttributeValue("type"));
        }
        if (e.getAttribute("description") != null) {
            setDescription(e.getAttributeValue("description"));
        }
        if (e.getAttribute("unit") != null) {
            setUnit(e.getAttributeValue("unit"));
        }
        if (e.getAttribute("exponent") != null) {
            setExponent(e.getAttributeValue("exponent"));
        }
        if (e.getAttribute("reference") != null) {
            this.setReferenceTypeID(e.getAttributeValue("reference"));
        }
        if (e.getText() != null && !e.getText().equals("")) {
            setValue(e.getText());
        }
        if (e.getAttribute("id") != null) {
            setId(Integer.parseInt(e.getAttributeValue("id")));
        }
        if (e.getAttribute("importance") != null) {
            setImportance(e.getAttributeValue("importance"));
        }
        if (e.getAttribute("generator") != null) {
            setGenerator(e.getAttributeValue("generator"));
        }
        if (e.getAttribute("timestamp") != null) {
            setTimestamp(e.getAttributeValue("timestamp"));
        }
        for (final Element ce : e.getChildren()) {
            if (ce.getName().equalsIgnoreCase("Property")) {
                getProperties().add(new Property(ce));
            } else if (ce.getName().equalsIgnoreCase("Parent")) {
                final Entity parent = new Entity() {

                    // Maybe extract separate class?
                    @Override
                    public Element createElement() {
                        final Element e = new Element("Parent");
                        e.setAttribute("id", Integer.toString(getId()));
                        e.setAttribute("name", getName());
                        e.setAttribute("description", getDescription());
                        return e;
                    }
                };
                parent.setFromElement(ce);
                addParent(parent);
            } else if (ce.getName().equalsIgnoreCase("Error")) {
                try {
                    getMessages().add(
                            new Message(ce.getAttribute("code").getIntValue(), ce
                                    .getAttributeValue("description"), ce.getText(),
                                    MessageType.ERROR));
                } catch (final DataConversionException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            } else if (ce.getName().equalsIgnoreCase("Warning")) {
                try {
                    getMessages().add(
                            new Message(ce.getAttribute("code").getIntValue(), ce
                                    .getAttributeValue("description"), ce.getText(),
                                    MessageType.WARNING));
                } catch (final DataConversionException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            } else if (ce.getName().equalsIgnoreCase("Info")) {
                try {

                    getMessages().add(
                            new Message(ce.getAttribute("code") != null ? ce.getAttribute("code")
                                    .getIntValue() : null, ce.getAttributeValue("description"), ce
                                    .getText(), MessageType.INFO));
                } catch (final DataConversionException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        }
        updateIndex();
    }

    /*
     * Property Index:
     */
    private HashMap<String, Integer> index = null;

    private void updateIndex() {
        index = new HashMap<String, Integer>();
        for (int i = 0; i < getProperties().size(); i++) {
            index.put(getProperties().get(i).getName(), i);
        }
    }

    public Entity getProperty(final String name) {
        if (index.containsKey(name)) {
            return getProperties().get(index.get(name));
        }
        return null;
    }

    /**
     * @return the generator
     */
    public String getGenerator() {
        return generator;
    }

    /**
     * @param generator
     *            the generator to set
     */
    public void setGenerator(final String generator) {
        this.generator = generator;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @return the properties
     */
    public LinkedList<Property> getProperties() {
        return properties;
    }

    /**
     * @param properties
     *            the properties to set
     */
    public void setProperties(final LinkedList<Property> properties) {
        this.properties = properties;
    }

    /**
     * @return the parents
     */
    public LinkedList<Entity> getParents() {
        return parents;
    }

    /**
     * @param parents
     *            the parents to set
     */
    public void setParents(final LinkedList<Entity> parents) {
        this.parents = parents;
    }

    /**
     * @param referenceTypeID
     *            the referenceTypeID to set
     */
    public void setReferenceTypeID(final Integer referenceTypeID) {
        this.referenceTypeID = referenceTypeID;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the type
     */
    public PropertyType getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        try {
            this.type = PropertyType.valueOf(type.toUpperCase());
        } catch (final IllegalArgumentException e) {
            this.type = PropertyType.USERDEFINED;
            this.type.setType(type);
        }

    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final PropertyType type) {
        this.type = type;
    }

    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit
     *            the unit to set
     */
    public void setUnit(final String unit) {
        this.unit = unit;
    }

    /**
     * @return the exponent
     */
    public Integer getExponent() {
        return exponent;
    }

    /**
     * @param exponent
     *            the exponent to set
     */
    public void setExponent(final Integer exponent) {
        this.exponent = exponent;
    }

    /**
     * @param exponent
     *            the exponent to set
     */
    public void setExponent(final String exponent) {
        this.exponent = Integer.parseInt(exponent);
    }

    /**
     * @return the importance
     */
    public String getImportance() {
        return importance;
    }

    /**
     * @param importance
     *            the importance to set
     */
    public void setImportance(final String importance) {
        this.importance = importance;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final Object value) {
        this.value = value;
    }

    /**
     * @return The ID of the RecordType this reference points to.
     */
    public Integer getReferenceTypeID() {
        return referenceTypeID;
    }

    /**
     * @param referenceTypeID
     *            The ID of the RecordType this reference points to.
     */
    public void setReferenceTypeID(final int referenceTypeID) {
        this.referenceTypeID = referenceTypeID;
    }

    /**
     * @param referenceTypeID
     *            The ID of the RecordType this reference points to (as a
     *            String).
     */
    public void setReferenceTypeID(final String referenceTypeIDString) {
        referenceTypeID = Integer.parseInt(referenceTypeIDString);
    }

    /**
     * @return the messages
     */
    public LinkedList<Message> getMessages() {
        return messages;
    }

    /**
     * @param messages
     *            the messages to set
     */
    public void setMessages(final LinkedList<Message> messages) {
        this.messages = messages;
    }

    /**
     * Add a property to this entity.
     * 
     * @param property
     * @author Timm Fitschen
     */
    public void addProperty(final Property property) {
        properties.add(property);
    }

    public void addProperty(final Integer id) {
        properties.add(new Property(id));
    }

    public void addProperty(final Integer id, final String importance) {
        final Property p = new Property(id);
        p.setImportance(importance);
        properties.add(p);
    }

    /**
     * Add a property to this entity with a certain importance.
     * 
     * @param property
     * @param importance
     * @author Timm Fitschen
     */
    public void addProperty(final Property property, final String importance) {
        property.setImportance(importance);
        properties.add(property);
    }

    /**
     * Add a parent to this entity.
     * 
     * @param parent
     * @author Timm Fitschen
     */
    public void addParent(final Entity parent) {
        parents.add(parent);
    }

    /**
     * Add a parent with ID = parId to this entity.
     * 
     * @param parId
     * @author Timm Fitschen
     */
    public void addParent(final Integer parId) {
        final Entity par = new Entity() {

            @Override
            public Element createElement() {
                return null;
            }
        };
        par.setId(parId);
        addParent(par);
    }

    /**
     * @param m
     * @author Timm Fitschen
     */
    public void addMessage(final Message m) {
        messages.add(m);
    }

    public void print() {
        System.out.println("ID: " + id);
        System.out.println("Name: " + name);
        System.out.println("Desc: " + description);
        if (type != null) {
            System.out.println("Type: " + type.toString());
        }
    }

    public String getString() {
        return getString("");
    }

    public String getString(final String indent) {
        final StringBuilder builder = new StringBuilder();
        builder.append(indent + "+---|" + this.getClass().getSimpleName() + "|-----------------\n");
        if (id != null) {
            builder.append(indent + "|          id: " + id.toString() + "\n");
        }
        if (name != null) {
            builder.append(indent + "|        name: " + name + "\n");
        }
        if (description != null) {
            builder.append(indent + "| description: " + description + "\n");
        }
        if (type != null) {
            builder.append(indent + "|        type: " + type.toString() + "\n");
        }
        if (exponent != null) {
            builder.append(indent + "|    exponent: " + exponent.toString() + "\n");
        }
        if (unit != null) {
            builder.append(indent + "|        unit: " + unit + "\n");
        }
        if (generator != null) {
            builder.append(indent + "|   generator: " + generator + "\n");
        }
        if (timestamp != null) {
            builder.append(indent + "|   timestamp: " + timestamp + "\n");
        }
        if (referenceTypeID != null) {
            builder.append(indent + "|       refid: " + referenceTypeID.toString() + "\n");
        }
        if (value != null) {
            builder.append(indent + "|       value: " + value.toString() + "\n");
        }
        for (final Message m : getMessages()) {
            builder.append(m.getString("|  "));
        }
        for (final Entity e : getParents()) {
            builder.append(e.getString("|  "));
        }
        for (final Entity e : getProperties()) {
            builder.append(e.getString("|  "));
        }
        return builder.toString();
    }

    /**
     * @return the cuid
     */
    public Integer getCuid() {
        return cuid;
    }

    /**
     * @param cuid
     *            the cuid to set
     */
    public void setCuid(final Integer cuid) {
        this.cuid = cuid;
    }
}
