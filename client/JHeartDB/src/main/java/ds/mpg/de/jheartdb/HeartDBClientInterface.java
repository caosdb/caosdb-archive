/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package ds.mpg.de.jheartdb;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;

import ds.mpg.de.jheartdb.HeartDBClient.DropOffBoxNotReachableException;
import ds.mpg.de.jheartdb.log.HTTPLogEntry;
import ds.mpg.de.jheartdb.structures.DBException;
import ds.mpg.de.jheartdb.structures.Entity;
import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Info;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.Record;
import ds.mpg.de.jheartdb.structures.RecordType;

/**
 * Interface to get a good overview and easy way to refactor things.
 * 
 * @author tf
 * 
 */
public interface HeartDBClientInterface {

    public void addLogEntry(HTTPLogEntry entry);

    public List<HTTPLogEntry> getLogList();

    public Document postXML(String xmlString, String uriSegment);

    public Info getServerInfo() throws InterruptedException;

    public boolean canConnect() throws IOException, URISyntaxException;

    public List<DBException> findExceptions(Element e, HashSet<Integer> ignoreExceptions);

    public List<DBException> findExceptions(Element e, int[] ignoreExceptions);

    public LinkedList<Record> retrieveRecord(Integer[] ids) throws InterruptedException;

    public LinkedList<Record> retrieveRecord(String[] names) throws InterruptedException;

    public LinkedList<Record> retrieveRecord(Integer id);

    public LinkedList<Record> retrieveRecord(Integer id, String name);

    public LinkedList<Record> retrieveRecord(Iterable<Integer> ids, Iterable<String> names);

    public LinkedList<Record> retrieveRecord(String str);

    public LinkedList<Record> deleteRecord(Integer[] ids);

    public LinkedList<Record> deleteRecord(String[] names);

    public LinkedList<Record> deleteRecord(Integer id);

    public LinkedList<Record> deleteRecord(Integer id, String name);

    public LinkedList<Record> deleteRecord(Iterable<Integer> ids, Iterable<String> names);

    public LinkedList<Record> deleteRecord(String str);

    public LinkedList<Record> updateRecord(Iterable<Record> updates);

    public LinkedList<Record> insertRecord(Record insert);

    public LinkedList<Record> insertRecord(Iterable<Record> inserts);

    public LinkedList<Entity> retrieveEntity(Integer... id);

    public LinkedList<RecordType> retrieveRecordType(Integer[] ids);

    public LinkedList<RecordType> retrieveRecordType(String[] names);

    public LinkedList<RecordType> retrieveRecordType(Integer id);

    public LinkedList<RecordType> retrieveRecordType(Integer id, String name);

    public LinkedList<RecordType> retrieveRecordType(Iterable<Integer> ids, Iterable<String> names);

    public LinkedList<RecordType> retrieveRecordType(String str);

    public LinkedList<RecordType> deleteRecordType(Integer[] ids);

    public LinkedList<RecordType> deleteRecordType(String[] names);

    public LinkedList<RecordType> deleteRecordType(Integer id);

    public LinkedList<RecordType> deleteRecordType(Integer id, String name);

    public LinkedList<RecordType> deleteRecordType(Iterable<Integer> ids, Iterable<String> names);

    public LinkedList<RecordType> deleteRecordType(String str);

    public LinkedList<RecordType> updateRecordType(Iterable<RecordType> updates);

    public LinkedList<RecordType> insertRecordType(RecordType insert);

    public LinkedList<RecordType> insertRecordType(Iterable<RecordType> inserts);

    public LinkedList<File> retrieveFile(Integer[] ids);

    public LinkedList<File> retrieveFile(String[] names);

    public LinkedList<File> retrieveFile(Integer id);

    public LinkedList<File> retrieveFile(Integer id, String name);

    public LinkedList<File> retrieveFile(Iterable<Integer> ids, Iterable<String> names);

    public LinkedList<File> retrieveFile(String str);

    public LinkedList<File> deleteFile(Integer[] ids);

    public LinkedList<File> deleteFile(String[] names);

    public LinkedList<File> deleteFile(Integer id);

    public LinkedList<File> deleteFile(Integer id, String name);

    public LinkedList<File> deleteFile(Iterable<Integer> ids, Iterable<String> names);

    public LinkedList<File> deleteFile(String str);

    public LinkedList<File> updateFile(Iterable<File> updates);

    public LinkedList<File> insertFile(Iterable<File> inserts)
            throws DropOffBoxNotReachableException;

    public LinkedList<File> insertFile(Iterable<File> inserts, int method)
            throws DropOffBoxNotReachableException;

    public LinkedList<File> insertFile(File insert) throws DropOffBoxNotReachableException;

    public LinkedList<File> insertFile(File insert, int method)
            throws DropOffBoxNotReachableException;

    public LinkedList<Property> retrieveProperty(Integer[] ids);

    public LinkedList<Property> retrieveProperty(String[] names);

    public LinkedList<Property> retrieveProperty(Integer id);

    public LinkedList<Property> retrieveProperty(Integer id, String name);

    public LinkedList<Property> retrieveProperty(Iterable<Integer> ids, Iterable<String> names);

    public LinkedList<Property> retrieveProperty(String str);

    public LinkedList<Property> deleteProperty(Integer[] ids);

    public LinkedList<Property> deleteProperty(String[] names);

    public LinkedList<Property> deleteProperty(Integer id);

    public LinkedList<Property> deleteProperty(Integer id, String name);

    public LinkedList<Property> deleteProperty(Iterable<Integer> ids, Iterable<String> names);

    public LinkedList<Property> deleteProperty(String str);

    public LinkedList<Property> updateProperty(Iterable<Property> updates);

    public LinkedList<Property> insertProperty(Property insert);

    public LinkedList<Property> insertProperty(Iterable<Property> inserts);

    public LinkedList<Record> deleteRecord(Iterable<? extends Object> ids);

    public LinkedList<File> updateFile(File update);

    public LinkedList<File> updateFile(File update, int method);

    public LinkedList<File> updateFile(Iterable<File> updates, int method);

}
