/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.mpg.de.jheartdb.structures;

/**
 *
 * @author salexan
 */
public class DBException extends Exception {

    private int errorCode;

    /**
     * Creates a new instance of
     * <code>DBException</code> without detail message.
     */
    public DBException() {
    }

    public DBException(String code, String description) {
        this("Database error " + code + ": " + description, Integer.parseInt(code));
    }

    /**
     * Constructs an instance of
     * <code>DBException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public DBException(String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of
     * <code>DBExceptions</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public DBException(String msg, int errorCode) {
        super(msg);
        this.errorCode = errorCode;
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }
}
