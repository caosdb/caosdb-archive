/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package ds.mpg.de.jheartdb.structures;

import org.jdom2.DataConversionException;
import org.jdom2.Element;

public class Message {
    public enum MessageType {
        ERROR, WARNING, INFO
    };

    private Integer code = null;
    private String description = null;
    private String body = null;
    private String type = null;

    public Message(final Element element) throws DataConversionException {
        setType(element.getName());
        if (element.getAttribute("code") != null) {
            setCode(element.getAttribute("code").getIntValue());
        }
        if (element.getAttribute("description") != null) {
            setDescription(element.getAttributeValue("description"));
        }
        if (element.getText() != null) {
            setBody(element.getText());
        }
    }

    public Message(final String type, final String body) {
        if (body == null || body.trim().equals("")) {
            this.body = null;
        } else {
            this.body = body;
        }
        this.type = type;
    }

    public Message(final Integer code, final String description, final String body,
            final MessageType type) {
        this.code = code;
        this.description = description;
        if (body == null || body.trim().equals("")) {
            this.body = null;
        } else {
            this.body = body;
        }
        this.type = type.toString();
    }

    /**
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final Integer code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final MessageType type) {
        this.type = type.toString();
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body
     *            the body to set
     */
    public void setBody(final String body) {
        if (body == null || body.trim().equals("")) {
            this.body = null;
        } else {
            this.body = body;
        }
    }

    /**
     * 
     * @author Timm Fitschen
     */
    public void print() {
        System.out.println("+--| " + type.toString() + " |-----------");
        if (code != null) {
            System.out.println("| (" + code.toString() + ") " + code.toString());
        }
        if (description != null) {
            System.out.println("| (" + code.toString() + ") " + description);
        }
        if (body != null) {
            System.out.println(body);
        }
        System.out.println("+----------------------------------------");
    }

    public String getString() {
        return getString("");
    }

    public String getString(final String indent) {
        final StringBuilder builder = new StringBuilder();
        builder.append(indent + "+---|" + type + "|-----------------\n");
        if (code != null) {
            builder.append(indent + "|        code: " + code.toString() + "\n");
        }
        if (description != null) {
            builder.append(indent + "| description: " + description + "\n");
        }
        if (body != null) {
            builder.append(indent + "|        body: " + body + "\n");
        }
        return builder.toString();
    }

    /**
     * @return
     * @author Timm Fitschen
     */
    public Element createElement() {
        final Element element = new Element(type);
        if (code != null) {
            element.setAttribute("code", code.toString());
        }
        if (description != null) {
            element.setAttribute("description", description);
        }
        if (body != null) {
            element.setText(body);
        }
        return element;
    }
}
