/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-16
 */

package ds.mpg.de.jheartdb.structures;

import org.jdom2.Element;

/**
 * Mapping from DB-type RecordType to a java class. Most of it is adapted from
 * class Entity.
 * 
 * @author salexan, Timm Fitschen
 */
public class RecordType extends Entity {

    public RecordType() {
    }

    public RecordType(final Element e) {
        this();
        setFromElement(e);
    }

    /**
     * @param name
     * @param description
     */
    public RecordType(final String name, final String description) {
        setName(name);
        setDescription(description);
    }

    @Override
    public Element createElement() {
        return createElement("RecordType");
    }

}
