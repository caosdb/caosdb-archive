/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-14
 */
package ds.mpg.de.jheartdb.structures;

import org.jdom2.Element;

/**
 * Mapping from DB properties to java class.
 * 
 * @author salexan
 */
public class Property extends Entity {

    public Property() {
    }

    public Property(final Integer id) {
        setId(id);
    }

    public Property(final String name, final String type, final String description,
            final String unit, final int id, final String importance, final Object value) {
        setName(name);
        setType(type);
        setDescription(description);
        setUnit(unit);
        setId(id);
        setImportance(importance);
        setValue(value);
    }

    public Property(final String name, final String type, final String description,
            final String unit, final String importance) {
        setName(name);
        setType(type);
        setDescription(description);
        setUnit(unit);
        setImportance(importance);
    }

    public Property(final Element e) {
        setFromElement(e);
    }

    /**
     * @param name
     * @param description
     * @param type
     */
    public Property(final String name, final String description, final String type) {
        super();
        setName(name);
        setDescription(description);
        setType(type);
    }

    /**
     * @param name
     * @param description
     * @param type
     */
    public Property(final String name, final String description, final PropertyType type) {
        super();
        setName(name);
        setDescription(description);
        setType(type);
    }

    @Override
    public Element createElement() {
        return createElement("Property");
    }

}
