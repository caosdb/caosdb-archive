/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package ds.mpg.de.jheartdb.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.util.EntityUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class Utilities {

    /**
     * Creates a HTTP-compatible range string ranging from "from" to "to"
     * (inclusive).
     * 
     * Example: buildEntityURISegment(4, 8) ... will result in: 4&5&6&7&8
     * 
     * @param from
     * @param to
     * @return
     */
    public static String buildEntityURISegment(final int from, final int to) {
        final StringBuilder b = new StringBuilder();
        for (int i = from; i <= to; i++) {
            b.append(i);
            if (i < to) {
                b.append("&");
            }
        }
        return b.toString();
    }

    public static String buildEntityURISegment(final Integer... ids) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (ids != null) {
            for (final Integer i : ids) {
                stringBuilder.append(i);
                stringBuilder.append('&');
            }
        }
        return stringBuilder.toString();
    }

    public static String buildEntityURISegment(final Integer id, final String name) {
        final StringBuilder stringBuilder = new StringBuilder(id);
        stringBuilder.append('&');
        stringBuilder.append(name);
        return stringBuilder.toString();
    }

    public static String buildEntityURISegment(final String... names) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (names != null) {
            for (final String i : names) {
                stringBuilder.append(i);
                stringBuilder.append('&');
            }
        }
        return stringBuilder.toString();
    }

    public static String buildEntityURISegment(final Iterable<? extends Object> ids) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (ids != null) {
            for (final Object i : ids) {
                stringBuilder.append(i);
                stringBuilder.append('&');
            }
        }
        return stringBuilder.toString();
    }

    public static String buildEntityURISegment(final Iterable<? extends Object> ids,
            final Iterable<? extends Object> names) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (ids != null) {
            for (final Object i : ids) {
                stringBuilder.append(i);
                stringBuilder.append('&');
            }
        }
        if (names != null) {
            for (final Object n : names) {
                stringBuilder.append(n);
                stringBuilder.append('&');
            }
        }
        return stringBuilder.toString();
    }

    public static Document inputStream2Document(final InputStream in) throws JDOMException,
            IOException {
        Document doc = null;
        final SAXBuilder builder = new SAXBuilder();
        builder.setIgnoringBoundaryWhitespace(true);
        doc = builder.build(in);
        return doc;
    }

    public static HttpEntity Document2HttpEntity(final Document document)
            throws UnsupportedEncodingException {
        return new StringEntity(Document2String(document), "UTF-8");
    }

    public static String Document2String(final Document document) {
        if (document != null && document.hasRootElement()) {
            final XMLOutputter outputter = new XMLOutputter();
            final Format newFormat = Format.getPrettyFormat();
            outputter.setFormat(newFormat);
            return outputter.outputString(document);
        }
        return "";
    }

    public static String Entity2String(final HttpEntity entity) {
        if (entity.isRepeatable()) {
            try {
                return EntityUtils.toString(entity);
            } catch (final ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return "Entity cannot be converted to String";
    }

    /**
     * Converts a FileItemStream into a String.
     * 
     * @param item
     * @return FileItemStream as a String.
     */
    public static String fileStreamToString(final FileItemStream item) {
        InputStream inputStream;
        try {
            inputStream = item.openStream();
            final StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
            return writer.toString();
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "Error!";
    }

    public static Long getSize(final File file) {
        if (file.isFile()) {
            return file.length();
        } else {
            Long size = 0L;
            for (final File child : file.listFiles()) {
                size += getSize(child);
            }
            return size;
        }
    }

    /**
     * Get the SHA-512 checksum of a file or a folder. The Checksum C of a
     * folder f is calculated by (recursively):<br>
     * C(f) = C(C(f_name) + C(f_subfile_1) + ... + C(f_subfile_N))<br>
     * while f_name is the (String) name of f and f_subfile_1 to f_subfile_N are
     * the alphabetically ordered sub files and sub folders of f. The
     * alphabetical order is defined by UTF16.<br>
     * 
     * @param file
     * @return SHA-512 Checksum
     * @throws IOException
     */
    public static String getChecksum(final File file) {
        try {
            if (file.isFile()) {
                return getChecksumSingleFile(file);
            } else {
                final StringBuffer buffer = new StringBuffer(file.getName());
                final String[] childNames = file.list();

                // This uses utf16 sorting.
                Arrays.sort(childNames);

                for (final String name : childNames) {
                    buffer.append(getChecksum(new File(file, name)));
                }
                return getChecksumString(buffer.toString());
            }
        } catch (final NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException @ FilesUtils.getChecksum(File file) - "
                    + e.getMessage());
            // throw e;
        } catch (final FileNotFoundException e) {
            System.out.println("FileNotFoundException @ FilesUtils.getChecksum(File file) - "
                    + e.getMessage());
            // throw e;
        } catch (final IOException e) {
            System.out.println("IOException @ FilesUtils.getChecksum(File file) - "
                    + e.getMessage());
            // throw e;
        }
        return null;
    }

    public static String hashPassword(final String str) throws NoSuchAlgorithmException {
        return getChecksumString(str);
    }

    private static String getChecksumString(final String str) throws NoSuchAlgorithmException {
        final MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(str.getBytes());
        final byte[] result = md.digest();
        return toHex(result);
    }

    private static String getChecksumSingleFile(final File file) throws NoSuchAlgorithmException,
            IOException {
        final InputStream stream = new FileInputStream(file);
        final MessageDigest md = MessageDigest.getInstance("SHA-512");
        final byte[] buf = new byte[1024];
        int size = 0;
        while ((size = stream.read(buf)) != -1) {
            md.update(buf, 0, size);
        }
        stream.close();
        final byte[] result = md.digest();
        return toHex(result);
    }

    /**
     * Method needed to get the Checksum.
     * 
     * @param b
     * @return
     */
    public static String toHex(final byte[] binary) {
        try {
            final StringBuffer buf = new StringBuffer(2 * binary.length + 1);
            for (final byte b : binary) {
                int nibble = b >> 4 & 0x0F;
                if (nibble >= 0 && nibble < 10) {
                    buf.append((char) ('0' + nibble));
                } else {
                    buf.append((char) ('a' + (nibble - 10)));
                }
                nibble = b & 0x0F;
                if (nibble >= 0 && nibble < 10) {
                    buf.append((char) ('0' + nibble));
                } else {
                    buf.append((char) ('a' + (nibble - 10)));
                }
            }
            return buf.toString();
        } catch (final Exception e) {
            System.out.println("EXCEPTION @ final Utilities.toHex(byte[] binary) - "
                    + e.getMessage());
        }
        return null;
    }

    public static boolean folderIsReachable(final String path, final String server) {
        final File f = new File(path);
        if (!f.exists()) {
            return false;
        }
        try {
            final InetAddress host = InetAddress.getByName(server);
            if (host.isLoopbackAddress()) {
                return true;
            }
        } catch (final UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public static HttpEntity FilesList2MultipartEntity(final Element rootElement,
            final List<ds.mpg.de.jheartdb.structures.File> uploadList) {
        try {
            // generate the first part of the multi-part post body - the xml
            // representation of all submitted files.

            final XMLOutputter outputter = new XMLOutputter();
            for (final ds.mpg.de.jheartdb.structures.File f : uploadList) {
                // calculate size if not done yet
                f.getSize();

                final Element fe = f.createElement();
                // if getPickup is null, the file is conventionally intendet to
                // be uploaded via http.
                // Set temporary upload identifier here
                fe.setAttribute("upload", f.getTempIdentifier());
                rootElement.addContent(fe);
            }
            final String outputString = outputter.outputString(rootElement);
            final StringBody stringBody = new StringBody(outputString, Charset.forName("UTF-8"));

            final MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("FileRepresentation", stringBody);

            for (final ds.mpg.de.jheartdb.structures.File f : uploadList) {
                final FileInputStream fileInputStream = new FileInputStream(f.getFile());
                final InputStreamBody inputStreamBody = new InputStreamBody(fileInputStream,
                        "application/octet-stream", f.getTempIdentifier());

                // Identify this file via its upload identifier
                multipartEntity.addPart(f.getTempIdentifier(), inputStreamBody);
            }

            return multipartEntity;
        } catch (final UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
