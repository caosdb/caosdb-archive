/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.structures.PropertyType.DOUBLE;
import static ds.mpg.de.jheartdb.structures.PropertyType.REFERENCE;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getPrefix;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRand;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRandString;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.setValue;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Test;

import ds.mpg.de.jheartdb.HeartDBClient;
import ds.mpg.de.jheartdb.HeartDBClient.DropOffBoxNotReachableException;
import ds.mpg.de.jheartdb.structures.Entity;
import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.PropertyType;
import ds.mpg.de.jheartdb.structures.Record;
import ds.mpg.de.jheartdb.structures.RecordType;

public class Bugs {

    @After
    public void after() throws IOException {
        HeartDBClientTest.printLog();
    }

    @Test
    public void Ticket61() {
        // insert good records
        final LinkedList<Property> properties = getHeartDBClient().retrieveProperty("all");
        final LinkedList<RecordType> recordTypes = getHeartDBClient().retrieveRecordType("all");

        final Record rec = new Record("HeartDBClientTest" + getRandString(), "HeartDBClientTest");
        final RecordType parent = recordTypes.get(getRand().nextInt(recordTypes.size()));

        rec.addParent(parent);
        for (final Property p : parent.getProperties()) {
            setValue(p);
            rec.addProperty(p);
        }
        Property p = properties.get(getRand().nextInt(properties.size()));
        setValue(p);
        rec.addProperty(p);
        p = properties.get(getRand().nextInt(properties.size()));
        setValue(p);
        rec.addProperty(p);

        LinkedList<Record> ret = getHeartDBClient().insertRecord(rec);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
        assertNotNull(ret.getFirst().getParents());
        assertFalse(ret.getFirst().getParents().isEmpty());
        assertEquals(parent.getId(), ret.getFirst().getParents().getFirst().getId());
        final Integer id = ret.getFirst().getId();

        // retrieve another time
        ret = HeartDBClientTest.getHeartDBClient().retrieveRecord(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
        assertNotNull(ret.getFirst().getParents());
        assertFalse(ret.getFirst().getParents().isEmpty());
        assertEquals(parent.getId(), ret.getFirst().getParents().getFirst().getId());
    }

    @Test
    public void Ticket62() {
        // insert plain Record
        final Record rec = new Record();
        rec.setName("HeartDBClientTest_Ticket62_PlainRecord_" + getRandString());
        final LinkedList<Record> ret = getHeartDBClient().insertRecord(rec);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertTrue(ret.getFirst().getMessages().size() > 0);
        assertFalse(ret.getFirst().getId() >= 100);

    }

    @Test
    public void Ticket68() {

        // insert Reference Property
        final Property p = new Property("HeartDBClientTest_Ticket68_ReferenceProperty_"
                + getRandString(), "HeartDBClientTest", "Reference");
        final LinkedList<RecordType> refTypes = getHeartDBClient().retrieveRecordType("all");
        Integer refid = null;
        if (refTypes != null) {
            assertNotNull(refTypes.getFirst());
            refid = refTypes.get(getRand().nextInt(refTypes.size())).getId();
            p.setReferenceTypeID(refid);
            final LinkedList<Property> ret = getHeartDBClient().insertProperty(p);
            assertNotNull(ret);
            assertEquals(1, ret.size());
            assertNotNull(ret.getFirst().getMessages());
            assertEquals(0, ret.getFirst().getMessages().size());
            assertNotNull(ret.getFirst().getId());
            p.setId(ret.getFirst().getId());
            assertNotNull(ret.getFirst().getName());
            assertNotNull(ret.getFirst().getType());
            assertNotNull(ret.getFirst().getDescription());
            assertEquals(refid, ret.getFirst().getReferenceTypeID());
            assertEquals(REFERENCE, ret.getFirst().getType());
        }
        refid = p.getReferenceTypeID();
        p.print();

        // insert RecordType with one Reference Property (id and importance
        // only)
        final RecordType rt = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        rt.addProperty(p.getId(), "obligatory");

        final LinkedList<RecordType> ret = getHeartDBClient().insertRecordType(rt);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
        final Entity recTyProp = ret.getFirst().getProperty(p.getName());

        // should have been transfered
        assertEquals(refid, recTyProp.getReferenceTypeID());

    }

    @Test
    public void Ticket70() {

        // insert Double Property
        final Property p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest",
                "Double");
        p.setUnit("m");
        final LinkedList<Property> ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        p.setId(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getUnit());
        final PropertyType ptype = p.getType();
        p.setUnit(ret.getFirst().getUnit());
        p.setName(null);
        p.setType((PropertyType) null);
        assertEquals("m", ret.getFirst().getUnit());
        assertEquals(DOUBLE, ret.getFirst().getType());
        // insert RecordType with one Reference Property (id and importance
        // only)

        final RecordType rt = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        rt.addProperty(p.getId(), "obligatory");

        final LinkedList<RecordType> ret2 = getHeartDBClient().insertRecordType(rt);
        assertNotNull(ret2);
        assertEquals(1, ret2.size());
        ret2.getFirst().print();
        assertNotNull(ret2.getFirst().getMessages());
        assertEquals(0, ret2.getFirst().getMessages().size());
        assertNotNull(ret2.getFirst().getId());
        assertTrue(ret2.getFirst().getId() >= 100);
        assertTrue(ret2.getFirst().getProperties().size() > 0);
        final Entity recTyProp = ret2.getFirst().getProperties().getFirst();
        assertNotNull(recTyProp);

        // should have been transfered
        assertEquals(ptype, recTyProp.getType());

    }

    /**
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws DropOffBoxNotReachableException
     */
    @Test
    public void Ticket80() throws UnsupportedEncodingException, IOException,
            DropOffBoxNotReachableException {
        // insert file type property
        Property p = new Property("HeartDBClientTest_Ticket80_FileProperty_" + getRandString(),
                "HeartDBClientTest", PropertyType.FILE);
        final LinkedList<Property> insertProperty = getHeartDBClient().insertProperty(p);
        p = insertProperty.getFirst();
        assertNotNull(p);
        assertTrue(p.getId() >= 100);
        final int filePropID = p.getId();

        // retrieve again and check datatype
        p = getHeartDBClient().retrieveProperty(filePropID).getFirst();
        assertEquals(PropertyType.FILE, p.getType());

        final String tStr = "./file.pickup";

        final java.io.File f = new java.io.File(tStr);
        f.deleteOnExit();

        final FileOutputStream out = new FileOutputStream(f, false);
        out.write(new String("asdf" + getRandString() + System.currentTimeMillis())
                .getBytes("UTF-8"));
        out.close();

        File file = new File(f, getPrefix() + "/" + getRandString() + ".test");
        file.setName("HeartDBClientTest_Ticket80_File_" + getRandString());

        final LinkedList<File> insertFile = getHeartDBClient().insertFile(file,
                HeartDBClient.DROP_OFF_BOX);

        file = insertFile.getFirst();

        // insert good recordType
        RecordType rt = new RecordType("HeartDBClientTest" + getRandString(), "HeartDBClientTest");

        rt.addProperty(p, "obligatory");

        final LinkedList<RecordType> insertRecordType = getHeartDBClient().insertRecordType(rt);
        assertNotNull(insertRecordType);
        assertEquals(1, insertRecordType.size());
        assertNotNull(insertRecordType.getFirst().getMessages());
        assertEquals(0, insertRecordType.getFirst().getMessages().size());
        assertNotNull(insertRecordType.getFirst().getId());
        assertTrue(insertRecordType.getFirst().getId() >= 100);

        rt = insertRecordType.getFirst();

        final Record rec = new Record("HeartDBClientTest" + getRandString(), "HeartDBClientTest");

        rec.addParent(rt);

        p.setValue(file);
        rec.addProperty(p);

        final LinkedList<Record> ret = getHeartDBClient().insertRecord(rec);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);

    }
}
