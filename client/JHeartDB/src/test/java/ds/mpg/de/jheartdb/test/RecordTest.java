/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/**
 * 
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRand;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRandString;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.hasNoErrorMessages;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.setValue;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

import java.io.IOException;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Test;

import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.PropertyType;
import ds.mpg.de.jheartdb.structures.Record;
import ds.mpg.de.jheartdb.structures.RecordType;

/**
 * @author Timm Fitschen
 * 
 */
public class RecordTest {
    @Test
    public void insertSingleRecord() {

        // insert good records
        final LinkedList<Property> properties = getHeartDBClient().retrieveProperty("all");
        final LinkedList<RecordType> recordTypes = getHeartDBClient().retrieveRecordType("all");

        final Record rec = new Record("HeartDBClientTest" + getRandString(), "HeartDBClientTest");
        final RecordType parent = recordTypes.get(getRand().nextInt(recordTypes.size()));

        rec.addParent(parent);
        for (final Property p : parent.getProperties()) {
            setValue(p);
            rec.addProperty(p);
        }
        Property p = properties.get(getRand().nextInt(properties.size()));
        setValue(p);
        rec.addProperty(p);
        p = properties.get(getRand().nextInt(properties.size()));
        setValue(p);
        rec.addProperty(p);

        final LinkedList<Record> ret = getHeartDBClient().insertRecord(rec);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
    }

    @Test
    public void insertSingleRecordWithCorruptPropertyValues() {

        final LinkedList<Property> properties = getHeartDBClient().retrieveProperty("all");
        final LinkedList<RecordType> recordTypes = getHeartDBClient().retrieveRecordType("all");
        final LinkedList<File> files = getHeartDBClient().retrieveFile("all");

        final Record rec = new Record("HeartDBClientTest" + getRandString(), "HeartDBClientTest");
        final RecordType parent = recordTypes.get(getRand().nextInt(recordTypes.size()));

        Property prop = properties.get(getRand().nextInt(properties.size()));
        rec.addProperty(prop);
        prop = properties.get(getRand().nextInt(properties.size()));
        rec.addProperty(prop);

        rec.addParent(parent);

        // add the unit and leave it empty. this once caused errors.
        for (final Property p : properties) {
            if (p.getId() == 21) {
                rec.addProperty(p);
            }
        }
        for (final Property p : parent.getProperties()) {
            rec.addProperty(p);
        }
        for (final Property p : rec.getProperties()) {
            switch (p.getType()) {
            case INTEGER:
                p.setValue("2.5");
                break;
            case DOUBLE:
                p.setValue("One point five.");
                break;
            case FILE:
                p.setValue("blablabla");
                break;
            case DATETIME:
                p.setValue("4 b.c.");
                break;
            case TIMESPAN:
                // submit a rational number instead of a floating point number.
                p.setValue("3/4h");
                break;
            case TEXT:
                // string cannot be set to a corrupt value...
                p.setValue("\n");
                break;
            case REFERENCE:
                p.setValue("blablabla");
                break;
            }
        }

        try{
        	final LinkedList<Record> ret = getHeartDBClient().insertRecord(rec);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        assertNotNull(ret.getFirst().getProperties());
        assertNotNull(ret.getFirst().getMessages());
        System.out.println(ret.getFirst());
        System.out.println(ret.getFirst().getMessages());
        System.out.println(hasNoErrorMessages(ret.getFirst()));
        assertFalse(hasNoErrorMessages(ret.getFirst()));
        for (final Property p : ret.getFirst().getProperties()) {
            if (p.getType() != PropertyType.TEXT) {
                assertFalse(hasNoErrorMessages(p));
            }
        }
        }catch (NumberFormatException exc){
        	insertSingleRecordWithCorruptPropertyValues();
        }
    }

    @After
    public void writeLog() throws IOException {
        HeartDBClientTest.printLog();
    }
}
