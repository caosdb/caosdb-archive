/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/**
 * 
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getPrefix;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRandString;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;

import org.jdom2.JDOMException;
import org.junit.After;
import org.junit.Test;

import ds.mpg.de.jheartdb.HeartDBClient;
import ds.mpg.de.jheartdb.HeartDBClient.DropOffBoxNotReachableException;
import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Message;
import ds.mpg.de.jheartdb.utils.Utilities;

/**
 * @author Timm Fitschen
 * 
 */
public class FileTest {

    @Test
    public void insertUploadFile() throws IOException, IllegalStateException, JDOMException,
            NoSuchAlgorithmException, URISyntaxException, InterruptedException,
            DropOffBoxNotReachableException {

        final String tStr = "./file.upload";

        final java.io.File f = new java.io.File(tStr);
        final FileOutputStream out = new FileOutputStream(f, false);
        out.write(new String("asdf" + getRandString() + System.currentTimeMillis())
                .getBytes("UTF-8"));
        out.close();

        File file = new File(f, getPrefix() + "/" + getRandString() + ".test");
        final String name = getPrefix() + "-File" + getRandString();
        file.setName(name);
        final LinkedList<File> ret = getHeartDBClient().insertFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        file = ret.getFirst();
        assertNotNull(file.getId());
        final int id = file.getId();
        assertTrue(id >= 100);
        assertNotNull(file.getSize());
        assertNotNull(file.getChecksum());
        assertNotNull(file.getMessages());
        assertTrue(HeartDBClientTest.hasNoErrorMessages(file));
        final java.io.File target = new java.io.File(tStr + "return");
        assertTrue(file.download(target));
        final java.io.File actualFile = file.getFile();
        assertNotNull(actualFile);
        assertTrue(actualFile.exists());
        final Long size = Utilities.getSize(actualFile);
        assertEquals(size, file.getSize());
        final String checksum = Utilities.getChecksum(actualFile);
        assertEquals(checksum, file.getChecksum());
    }

    @Test
    public void insertDropFile() throws IOException, IllegalStateException, JDOMException,
            NoSuchAlgorithmException, URISyntaxException, InterruptedException {

        final String tStr = "./file.pickup";

        final java.io.File f = new java.io.File(tStr);
        final FileOutputStream out = new FileOutputStream(f, false);
        out.write(new String("asdf" + getRandString() + System.currentTimeMillis())
                .getBytes("UTF-8"));
        out.close();

        File file = new File(f, getPrefix() + "/" + getRandString() + ".test");
        final String name = getPrefix() + "-File" + getRandString();
        file.setName(name);
        try {
            final LinkedList<File> ret = getHeartDBClient().insertFile(file,
                    HeartDBClient.DROP_OFF_BOX);
            assertNotNull(ret);
            assertEquals(1, ret.size());
            assertNotNull(ret.getFirst());
            file = ret.getFirst();
            assertNotNull(file.getId());
            final int id = file.getId();
            assertNotNull(file.getSize());
            assertNotNull(file.getChecksum());
            assertNotNull(file.getMessages());
            assertTrue(HeartDBClientTest.hasNoErrorMessages(file));
            final java.io.File target = new java.io.File(tStr + "return");
            assertTrue(file.download(target));
            final java.io.File actualFile = file.getFile();
            assertNotNull(actualFile);
            assertTrue(actualFile.exists());
            final Long size = Utilities.getSize(actualFile);
            assertEquals(size, file.getSize());
            final String checksum = Utilities.getChecksum(actualFile);
            assertEquals(checksum, file.getChecksum());
        } catch (final DropOffBoxNotReachableException e) {
            System.out.println("Skipping DropOff Test.");
            return;
        }
    }

    @Test
    public void insertAutomaticFile() throws IOException, IllegalStateException, JDOMException,
            NoSuchAlgorithmException, URISyntaxException, InterruptedException,
            DropOffBoxNotReachableException {

        final String tStr = "./file.auto";

        final java.io.File f = new java.io.File(tStr);
        final FileOutputStream out = new FileOutputStream(f, false);
        out.write(new String("asdf" + getRandString() + System.currentTimeMillis())
                .getBytes("UTF-8"));
        out.close();

        File file = new File(f, getPrefix() + "/" + getRandString() + ".test");
        final String name = getPrefix() + "-File" + getRandString();
        file.setName(name);
        final LinkedList<File> ret = getHeartDBClient().insertFile(file, HeartDBClient.AUTOMATIC);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        file = ret.getFirst();
        assertNotNull(file.getId());
        final int id = file.getId();
        assertNotNull(file.getSize());
        assertNotNull(file.getChecksum());
        assertNotNull(file.getMessages());
        assertTrue(HeartDBClientTest.hasNoErrorMessages(file));
        final java.io.File target = new java.io.File(tStr + "return");
        assertTrue(file.download(target));
        final java.io.File actualFile = file.getFile();
        assertNotNull(actualFile);
        assertTrue(actualFile.exists());
        final Long size = Utilities.getSize(actualFile);
        assertEquals(size, file.getSize());
        final String checksum = Utilities.getChecksum(actualFile);
        assertEquals(checksum, file.getChecksum());
    }

    @Test
    public void updateMetaDataUploadFile() throws UnsupportedEncodingException, IOException,
            URISyntaxException, IllegalStateException, JDOMException, InterruptedException,
            DropOffBoxNotReachableException {
        final String tStr = "./file.upload";

        final java.io.File f = new java.io.File(tStr);
        FileOutputStream out = new FileOutputStream(f, false);
        out.write(new String("asdf" + getRandString() + System.currentTimeMillis())
                .getBytes("UTF-8"));
        out.close();

        String location = getPrefix() + "/" + getRandString() + ".test";
        File file = new File(f, location);
        String name = getPrefix() + "-File" + getRandString();
        file.setName(name);
        LinkedList<File> ret = getHeartDBClient().insertFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        file = ret.getFirst();
        assertNotNull(file.getId());
        assertEquals(name, file.getName());
        final int id = file.getId();
        assertNotNull(file.getMessages());
        assertNotNull(file.getSize());
        assertNotNull(file.getChecksum());
        assertTrue(HeartDBClientTest.hasNoErrorMessages(file));
        java.io.File target = new java.io.File(tStr + "return");
        assertTrue(file.download(target));
        java.io.File actualFile = file.getFile();
        assertNotNull(actualFile);
        assertTrue(actualFile.exists());
        Long size = Utilities.getSize(actualFile);
        assertEquals(size, file.getSize());
        String checksum = Utilities.getChecksum(actualFile);
        assertEquals(checksum, file.getChecksum());

        // update name
        name = name + "update";
        file.setName(name);
        ret = getHeartDBClient().updateFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        file = ret.getFirst();
        assertNotNull(file.getId());
        assertEquals(name, file.getName());
        assertTrue(file.getId().intValue() == id);
        assertNotNull(file.getMessages());
        assertNotNull(file.getSize());
        assertNotNull(file.getChecksum());
        assertTrue(HeartDBClientTest.hasNoErrorMessages(file));
        target = new java.io.File(tStr + "return");
        assertTrue(file.download(target));
        actualFile = file.getFile();
        assertNotNull(actualFile);
        assertTrue(actualFile.exists());
        size = Utilities.getSize(actualFile);
        assertEquals(size, file.getSize());
        checksum = Utilities.getChecksum(actualFile);
        assertEquals(checksum, file.getChecksum());

        // change location
        assertEquals(location, file.getPath());
        location = getPrefix() + "/" + getRandString() + "test-upload";
        file.setPath(location);
        ret = getHeartDBClient().updateFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        file = ret.getFirst();
        assertNotNull(file.getId());
        assertEquals(location, file.getPath());
        assertTrue(file.getId().intValue() == id);
        assertNotNull(file.getMessages());
        assertNotNull(file.getSize());
        assertNotNull(file.getChecksum());
        assertTrue(HeartDBClientTest.hasNoErrorMessages(file));
        target = new java.io.File(tStr + "return");
        assertTrue(file.download(target));
        actualFile = file.getFile();
        assertNotNull(actualFile);
        assertTrue(actualFile.exists());
        size = Utilities.getSize(actualFile);
        assertEquals(size, file.getSize());
        checksum = Utilities.getChecksum(actualFile);
        assertEquals(checksum, file.getChecksum());

        // change file -> append stuff
        out = new FileOutputStream(actualFile, true);
        out.write(new String("updated").getBytes("UTF-8"));
        out.close();

        checksum = Utilities.getChecksum(actualFile);
        size = Utilities.getSize(actualFile);
        file.setFile(actualFile);
        file.setChecksum(checksum);
        file.setSize(size);
        ret = getHeartDBClient().updateFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        file = ret.getFirst();
        assertNotNull(file.getId());
        assertEquals(location, file.getPath());
        assertTrue(file.getId().intValue() == id);
        assertNotNull(file.getMessages());
        assertNotNull(file.getSize());
        assertNotNull(file.getChecksum());
        assertTrue(HeartDBClientTest.hasNoErrorMessages(file));
        assertEquals(size, file.getSize());
        assertEquals(checksum, file.getChecksum());

        target = new java.io.File(tStr + "return");
        assertTrue(file.download(target));
        actualFile = file.getFile();
        assertNotNull(actualFile);
        assertTrue(actualFile.exists());
        size = Utilities.getSize(actualFile);
        checksum = Utilities.getChecksum(actualFile);
        assertEquals(size, file.getSize());
        assertEquals(checksum, file.getChecksum());
    }

    @Test
    public void deleteFile() throws IOException, URISyntaxException, IllegalStateException,
            JDOMException, InterruptedException, DropOffBoxNotReachableException {
        final String tStr = "./file.upload";

        final java.io.File f = new java.io.File(tStr);
        final FileOutputStream out = new FileOutputStream(f, false);
        out.write(new String("asdf" + getRandString() + System.currentTimeMillis())
                .getBytes("UTF-8"));
        out.close();

        final String location = getPrefix() + "/" + getRandString() + ".test";
        File file = new File(f, location);
        final String name = getPrefix() + "-File" + getRandString();
        file.setName(name);
        LinkedList<File> ret = getHeartDBClient().insertFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        file = ret.getFirst();
        assertNotNull(file.getId());
        assertEquals(name, file.getName());
        final int id = file.getId();
        assertNotNull(file.getMessages());
        assertNotNull(file.getSize());
        assertNotNull(file.getChecksum());
        assertTrue(HeartDBClientTest.hasNoErrorMessages(file));
        java.io.File target = new java.io.File(tStr + "return");
        assertTrue(file.download(target));
        final java.io.File actualFile = file.getFile();
        assertNotNull(actualFile);
        assertTrue(actualFile.exists());
        final Long size = Utilities.getSize(actualFile);
        assertEquals(size, file.getSize());
        final String checksum = Utilities.getChecksum(actualFile);
        assertEquals(checksum, file.getChecksum());

        // delete file
        ret = getHeartDBClient().deleteFile(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst());
        file = ret.getFirst();
        assertNotNull(file.getId());
        assertEquals(name, file.getName());
        assertTrue(HeartDBClientTest.hasNoErrorMessages(file));

        // try to download file
        target = new java.io.File(tStr + "return");
        assertFalse(file.download(target));
        assertFalse(HeartDBClientTest.hasNoErrorMessages(file));
        boolean found401 = false;
        for (final Message m : file.getMessages()) {
            if (m.getCode() != null) {
                if (m.getCode().equals(401)) {
                    found401 = true;
                    break;
                }
            }
        }
        assertTrue(found401);
    }

    @After
    public void write() throws IOException {
        HeartDBClientTest.printLog();
    }

}
