/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package ds.mpg.de.jheartdb.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ds.mpg.de.jheartdb.HeartDBClient;
import ds.mpg.de.jheartdb.log.HTTPLogEntry;
import ds.mpg.de.jheartdb.structures.Entity;
import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Message;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.RecordType;

/**
 * 
 * @author Timm Fitschen
 */
@RunWith(Suite.class)
@SuiteClasses({ ConnectionTest.class, FileTest.class,
        // SQLiteTest.class,
        PropertyTest.class, RecordTypeTest.class, RecordTest.class, EntityContainerTest.class,
        Bugs.class, CleanUp.class })
public class HeartDBClientTest {

    private static final String username = "timm";
    private static final String password = "1234";
    private static final String host = "localhost";
    private static final int port = 8123;
    private static final String basepath = "mpidsserver";
    private static final Random rand = new Random(System.currentTimeMillis());
    private static final String prefix = "HeartDBClientTest";

    /**
     * @return the rand
     */
    public static Random getRand() {
        return rand;
    }

    /**
     * @return the username
     */
    public static String getUsername() {
        return username;
    }

    /**
     * @return the password
     */
    public static String getPassword() {
        return password;
    }

    /**
     * @return the host
     */
    public static String getHost() {
        return host;
    }

    /**
     * @return the port
     */
    public static int getPort() {
        return port;
    }

    /**
     * @return the basepath
     */
    public static String getBasepath() {
        return basepath;
    }

    /**
     * @return the prefix
     */
    public static String getPrefix() {
        return prefix;
    }

    private static HeartDBClient heartDBClient = null;

    /**
     * @return the heartDBClient
     */
    public static HeartDBClient getHeartDBClient() {
        if (heartDBClient == null) {
            heartDBClient = HeartDBClient.getInstance();
            heartDBClient.configureConnection(host, port, basepath, username, password);
        }
        return heartDBClient;
    }

    public static String getRandString() {
        return Integer.toHexString(rand.nextInt()) + Integer.toHexString(rand.nextInt())
                + Integer.toHexString(rand.nextInt());
    }

    private static FileOutputStream fileOut = null;

    private static boolean logFileThere() throws IOException {
        if (fileOut == null) {
            final java.io.File logFile = new java.io.File("./test.log");
            while (!logFile.createNewFile()) {
                logFile.delete();
            }
            fileOut = new FileOutputStream(logFile);
        }
        return true;
    }

    public static void log(final String log) throws IOException {
        if (logFileThere()) {

            final String logNL = log + "\n";
            System.out.print(logNL);
            fileOut.write(logNL.getBytes("UTF-8"));
        }
    }

    public static void printLog() throws IOException {
        final List<HTTPLogEntry> log = heartDBClient.getLogList();
        synchronized (log) {
            for (final HTTPLogEntry l : log) {
                log("==========================");
                log(l.toString());
                log(l.getAddress());
                log("========= Request ========");
                log(l.getSendInfo());
                log("========= Response =======");
                log(l.getRespInfo());
            }
        }
    }

    public static void setValue(final Property p) {
        final LinkedList<RecordType> recordTypes = heartDBClient.retrieveRecordType("all");
        final LinkedList<File> files = heartDBClient.retrieveFile("all");
        switch (p.getType()) {
        case INTEGER:
            p.setValue(rand.nextInt());
            break;
        case DOUBLE:
            p.setValue(rand.nextDouble());
            break;
        case FILE:
            p.setValue(files.get(rand.nextInt(files.size())).getId());
            break;
        case DATETIME:
            p.setValue(new Timestamp(System.currentTimeMillis()));
            break;
        case TIMESPAN:
            p.setValue(Float.toString(rand.nextFloat()));
            break;
        case TEXT:
            p.setValue(getRandString());
            break;
        case REFERENCE:
            p.setValue(recordTypes.get(rand.nextInt(recordTypes.size())).getId());
            break;
        case USERDEFINED:
            // do nothing
            break;
        default:
            break;
        }
    }

    public static boolean hasNoErrorMessages(final Entity e) {
        for (final Message m : e.getMessages()) {
            if (m.getType().equalsIgnoreCase("Error")) {
                return false;
            }
        }
        return true;
    }

}
