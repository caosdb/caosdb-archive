/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/**
 * 
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getBasepath;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHost;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getPassword;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getPort;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getUsername;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Test;

/**
 * @author Timm Fitschen
 * 
 */
public class ConnectionTest {

    @Test
    public void testCanConnect() throws IOException, URISyntaxException {
        System.out.println("Test wrong password.");
        getHeartDBClient().configureConnection(getHost(), getPort(), getBasepath(), getUsername(),
                "bla");
        assertTrue(getHeartDBClient().canConnect());

        System.out.println("Test wrong user name.");
        getHeartDBClient().configureConnection(getHost(), getPort(), getBasepath(), "bla",
                getPassword());
        assertTrue(getHeartDBClient().canConnect());

        System.out.println("Test wrong base path.");
        getHeartDBClient().configureConnection(getHost(), getPort(), "asdf", getUsername(),
                getPassword());
        assertNotNull(getHeartDBClient());
        assertTrue(getHeartDBClient().canConnect());

        // System.out.println("Test wrong port.");
        // getHeartDBClient().configureConnection(getHost(), 9999,
        // getBasepath(), getUsername(),
        // getPassword());
        // assertFalse(getHeartDBClient().canConnect());
        //
        // System.out.println("Test wrong host.");
        // getHeartDBClient().configureConnection("blablabla", getPort(),
        // getBasepath(),
        // getUsername(), getPassword());
        // assertFalse(getHeartDBClient().canConnect());

        System.out.println("Test correct configuration.");
        getHeartDBClient().configureConnection(getHost(), getPort(), getBasepath(), getUsername(),
                getPassword());
        assertTrue(getHeartDBClient().canConnect());
    }
}
