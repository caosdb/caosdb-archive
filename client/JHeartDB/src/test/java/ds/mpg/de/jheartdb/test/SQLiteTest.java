/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/**
 * 
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRandString;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.hasNoErrorMessages;
import static java.util.logging.Level.SEVERE;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Test;

import ds.mpg.de.jheartdb.HeartDBClient;
import ds.mpg.de.jheartdb.HeartDBClient.DropOffBoxNotReachableException;
import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Message;
import ds.mpg.de.jheartdb.structures.PropertyType;
import ds.mpg.de.jheartdb.utils.Utilities;

/**
 * @author Timm Fitschen
 * 
 */
public class SQLiteTest {

    static Connection con = null;
    static String database = "./test.sqlite";

    public static synchronized void createTestDB() throws ClassNotFoundException, SQLException {
        final java.io.File file = new java.io.File(database);
        if (file.exists()) {
            file.delete();
        }
        assertTrue(!file.exists());

        con = getConnection(database);
        final Statement stmt = con.createStatement();
        final String sql = "CREATE TABLE Test (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT NOT NULL);";
        stmt.executeUpdate(sql);
        stmt.close();
        con.close();
        final java.io.File f = new java.io.File(database);
        Assert.assertTrue(f.exists());
    }

    @Test
    public void insertAsFile() throws ClassNotFoundException, SQLException,
            DropOffBoxNotReachableException {
        // create db
        createTestDB();

        final java.io.File f = new java.io.File(database);
        final String checksum = Utilities.getChecksum(f);
        final Long size = Utilities.getSize(f);
        File file = new File(f, "testsqldatabase/" + getRandString() + ".sql.test");
        file.setType("SQLite");
        file.setName(HeartDBClientTest.getPrefix() + "__SQLite__" + getRandString());
        file.setDescription("HeartDBClientTest SQLite");
        LinkedList<File> ret = getHeartDBClient().insertFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        file = ret.getFirst();
        assertNotNull(file.getMessages());
        assertEquals(0, file.getMessages().size());
        assertNotNull(file.getId());
        assertTrue(file.getId() >= 100);
        assertNotNull(file.getType());
        assertTrue(file.getType() == PropertyType.USERDEFINED);
        assertTrue(file.getType().toString().equalsIgnoreCase("SQLite"));
        assertNotNull(file.getChecksum());
        assertNotNull(file.getSize());
        assertEquals(checksum, file.getChecksum());
        assertEquals(size, file.getSize());

        for (final Message m : file.getMessages()) {
            m.print();
        }

        file.addMessage(new Message("execute", "INSERT INTO Test (name) VALUES ('Timm')"));
        file.print();

        ret = getHeartDBClient().updateFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        file = ret.getFirst();
        assertNotNull(file.getMessages());
        assertTrue(hasNoErrorMessages(file));
        assertNotNull(file.getId());
        assertTrue(file.getId() >= 100);
        assertNotNull(file.getType());
        assertTrue(file.getType() == PropertyType.USERDEFINED);
        assertTrue(file.getType().toString().equalsIgnoreCase("SQLite"));
        assertNotNull(file.getChecksum());
        assertNotNull(file.getSize());
        assertFalse(checksum.equalsIgnoreCase(file.getChecksum()));

        for (final Message m : file.getMessages()) {
            m.print();
        }

        file.addMessage(new Message("execute", "Select * from Test"));
        ret = getHeartDBClient().updateFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        file = ret.getFirst();
        assertNotNull(file.getMessages());
        assertTrue(hasNoErrorMessages(file));
        assertNotNull(file.getId());
        assertTrue(file.getId() >= 100);
        assertNotNull(file.getType());
        assertTrue(file.getType() == PropertyType.USERDEFINED);
        assertTrue(file.getType().toString().equalsIgnoreCase("SQLite"));
        assertNotNull(file.getChecksum());
        assertNotNull(file.getSize());

        for (final Message m : file.getMessages()) {
            m.print();
        }
    }

    public static Connection getConnection(final String database) throws ClassNotFoundException,
            SQLException {
        Class.forName("org.sqlite.JDBC");
        final Connection con = DriverManager.getConnection("jdbc:sqlite:" + database);
        return con;
    }

    // multithreading test
    @Test
    public void multi() throws Throwable {
        final java.io.File f = new java.io.File(database);
        final String checksum = Utilities.getChecksum(f);
        final Long size = Utilities.getSize(f);
        File file = new File(f, "testsqldatabase/" + getRandString() + ".sql.test");
        file.setType("SQLite");
        file.setName(HeartDBClientTest.getPrefix() + "__SQLite__" + getRandString());
        file.setDescription("HeartDBClientTest SQLite");
        final LinkedList<File> ret = getHeartDBClient().insertFile(file, HeartDBClient.HTTP_UPLOAD);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        file = ret.getFirst();
        assertNotNull(file.getMessages());
        assertTrue(hasNoErrorMessages(file));
        assertNotNull(file.getId());
        assertTrue(file.getId() >= 100);
        assertNotNull(file.getType());
        assertTrue(file.getType() == PropertyType.USERDEFINED);
        assertTrue(file.getType().toString().equalsIgnoreCase("SQLite"));
        assertNotNull(file.getChecksum());
        assertNotNull(file.getSize());
        assertEquals(checksum, file.getChecksum());
        assertEquals(size, file.getSize());

        for (final Message m : file.getMessages()) {
            m.print();
        }

        final int runs = 100;
        final Thread[] threads = new Thread[runs];
        logger = Logger.getLogger(SQLiteTest.class.getName());
        final FileHandler fh = new FileHandler("./sqltest.log");
        fh.setFormatter(new SimpleFormatter());
        logger.addHandler(fh);
        for (int i = 0; i < runs; i++) {
            final multiinsert m = new multiinsert(file.getId(),
                    "INSERT INTO Test (ID, NAME) VALUES (" + i + ",'" + getRandString() + "')");
            final UncaughtExceptionHandler eh = new UncaughtExceptionHandler() {

                @Override
                public void uncaughtException(final Thread t, final Throwable e) {
                    Logger.getLogger(SQLiteTest.class.getName()).log(SEVERE,
                            Long.toHexString(t.getId()), e);
                    SQLiteTest.ok = false;
                }
            };
            m.setUncaughtExceptionHandler(eh);
            m.setName("Update-" + i);
            m.start();
            threads[i] = m;
        }
        for (final Thread m : threads) {
            m.join();
        }

        assertTrue(ok);
    }

    private static volatile boolean ok = true;
    private static Logger logger = null;

    class multiinsert extends Thread {
        private final int id;
        private final String statement;

        @Override
        public void run() {

            Logger.getLogger(SQLiteTest.class.getName()).info(
                    "Started Thread " + Long.toHexString(getId()) + "! -- Statement: " + statement);
            LinkedList<File> ret = HeartDBClientTest.getHeartDBClient().retrieveFile(id);
            assertNotNull(ret);
            assertEquals(1, ret.size());
            File file = ret.getFirst();
            assertNotNull(file.getMessages());
            assertTrue(hasNoErrorMessages(file));
            assertNotNull(file.getId());
            assertTrue(file.getId() >= 100);
            // assertNotNull(file.getType());
            // assertTrue(file.getType() == PropertyType.USERDEFINED);
            // assertTrue(file.getType().toString().equalsIgnoreCase("SQLite"));
            assertNotNull(file.getChecksum());
            assertNotNull(file.getSize());
            file.setType("SQLite");
            file.addMessage(new Message("execute", statement));

            ret = HeartDBClientTest.getHeartDBClient().updateFile(file);
            assertNotNull(ret);
            assertEquals(1, ret.size());
            file = ret.getFirst();
            assertNotNull(file.getMessages());
            assertTrue(hasNoErrorMessages(file));
            assertNotNull(file.getId());
            assertTrue(file.getId() >= 100);
            // assertNotNull(file.getType());
            // assertTrue(file.getType() == PropertyType.USERDEFINED);
            // assertTrue(file.getType().toString().equalsIgnoreCase("SQLite"));
            file.setType("SQLite");
            assertNotNull(file.getChecksum());
            assertNotNull(file.getSize());

            file.addMessage(new Message("execute", "Select * from Test"));
            ret = getHeartDBClient().updateFile(file, HeartDBClient.HTTP_UPLOAD);
            assertNotNull(ret);
            assertEquals(1, ret.size());
            file = ret.getFirst();
            assertNotNull(file.getMessages());
            assertTrue(hasNoErrorMessages(file));
            assertNotNull(file.getId());
            assertTrue(file.getId() >= 100);
            // assertNotNull(file.getType());
            // assertTrue(file.getType() == PropertyType.USERDEFINED);
            // assertTrue(file.getType().toString().equalsIgnoreCase("SQLite"));
            file.setType("SQLite");
            assertNotNull(file.getChecksum());
            assertNotNull(file.getSize());

        }

        public multiinsert(final int id, final String statement) {
            this.id = id;
            this.statement = statement;
        }
    }

    @After
    public void writeLog() throws IOException {
        HeartDBClientTest.printLog();
    }
}
