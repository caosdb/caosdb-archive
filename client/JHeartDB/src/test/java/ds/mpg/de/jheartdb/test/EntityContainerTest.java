/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/**
 * 
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.structures.PropertyType.DATETIME;
import static ds.mpg.de.jheartdb.structures.PropertyType.DOUBLE;
import static ds.mpg.de.jheartdb.structures.PropertyType.FILE;
import static ds.mpg.de.jheartdb.structures.PropertyType.INTEGER;
import static ds.mpg.de.jheartdb.structures.PropertyType.REFERENCE;
import static ds.mpg.de.jheartdb.structures.PropertyType.TEXT;
import static ds.mpg.de.jheartdb.structures.PropertyType.TIMESPAN;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRand;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRandString;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import ds.mpg.de.jheartdb.structures.Entity;
import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.Record;
import ds.mpg.de.jheartdb.structures.RecordType;

/**
 * @author Timm Fitschen
 * 
 */
public class EntityContainerTest {

    @Test
    public void retrieveProperty() {
        // retrieve all properties via the normal way
        final LinkedList<Property> properties = getHeartDBClient().retrieveProperty("all");

        // choose one id to be retrieved
        final int pid = properties.get(HeartDBClientTest.getRand().nextInt(properties.size()))
                .getId();

        // retrieve as entity
        final Entity p = HeartDBClientTest.getHeartDBClient().retrieveEntity(pid).getFirst();
        assertNotNull(p);

        // should be a property
        Assert.assertTrue(p instanceof Property);
    }

    @Test
    public void retrieveRecordType() {
        // retrieve all recordtypes via the normal way
        final LinkedList<RecordType> recordTypes = getHeartDBClient().retrieveRecordType("all");

        // choose one id to be retrieved
        final int rtid = recordTypes.get(HeartDBClientTest.getRand().nextInt(recordTypes.size()))
                .getId();

        // retrieve as entity
        final Entity rt = HeartDBClientTest.getHeartDBClient().retrieveEntity(rtid).getFirst();
        assertNotNull(rt);

        // should be a RecordType
        Assert.assertTrue(rt instanceof RecordType);
    }

    @Test
    public void retrieveRecord() {
        // retrieve all records via the normal way
        final LinkedList<Record> records = getHeartDBClient().retrieveRecord("all");

        if (records == null) {
            return;
        }
        // choose one id to be retrieved
        final int rid = records.get(HeartDBClientTest.getRand().nextInt(records.size())).getId();

        // retrieve as entity
        final Entity r = HeartDBClientTest.getHeartDBClient().retrieveEntity(rid).getFirst();
        assertNotNull(r);

        // should be a Record
        Assert.assertTrue(r instanceof Record);
    }

    @Test
    public void retrieveFile() {
        // retrieve all files via the normal way
        final LinkedList<File> files = getHeartDBClient().retrieveFile("all");

        if (files == null) {
            return;
        }

        // choose one id to be retrieved
        final int fid = files.get(HeartDBClientTest.getRand().nextInt(files.size())).getId();

        // retrieve as entity
        final Entity f = HeartDBClientTest.getHeartDBClient().retrieveEntity(fid).getFirst();
        assertNotNull(f);

        // should be a FIle
        Assert.assertTrue(f instanceof File);
    }

    @Test
    public void insertProperty() {
        // Insert good integer property
        Property p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest",
                "Integer");
        assertNotNull(p.getType());
        assertEquals("Integer".toUpperCase(), p.getType().toString().toUpperCase());
        p.setUnit("m");
        LinkedList<Entity> ret = getHeartDBClient().insertEntity(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getUnit());
        assertEquals("m", ret.getFirst().getUnit());
        assertEquals(INTEGER, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // Insert good double property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Double");
        p.setUnit("m");
        ret = getHeartDBClient().insertEntity(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getUnit());
        assertEquals("m", ret.getFirst().getUnit());
        assertEquals(DOUBLE, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // Insert good text property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Text");
        ret = getHeartDBClient().insertEntity(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(TEXT, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // Insert good datetime property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Datetime");
        ret = getHeartDBClient().insertEntity(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(DATETIME, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // Insert good timespan property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Timespan");
        p.setUnit("h");
        ret = getHeartDBClient().insertEntity(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getUnit());
        assertEquals("h", ret.getFirst().getUnit());
        assertEquals(TIMESPAN, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // Insert good file property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "File");
        ret = getHeartDBClient().insertEntity(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(FILE, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // Insert good reference property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Reference");
        final LinkedList<RecordType> refTypes = getHeartDBClient().retrieveRecordType("all");
        if (refTypes != null) {
            assertNotNull(refTypes.getFirst());
            final Integer refid = refTypes.get(getRand().nextInt(refTypes.size())).getId();
            p.setReferenceTypeID(refid);
            ret = getHeartDBClient().insertEntity(p);
            assertNotNull(ret);
            assertEquals(1, ret.size());
            assertNotNull(ret.getFirst().getMessages());
            assertEquals(0, ret.getFirst().getMessages().size());
            assertNotNull(ret.getFirst().getId());
            assertNotNull(ret.getFirst().getName());
            assertNotNull(ret.getFirst().getType());
            assertNotNull(ret.getFirst().getDescription());
            assertEquals(refid, ret.getFirst().getReferenceTypeID());
            assertEquals(REFERENCE, ret.getFirst().getType());
            assertTrue(ret.getFirst() instanceof Property);
        }
    }

    @Test
    public void insertPropertyWithParent() {
        // Insert good integer property
        final Property pPar = new Property("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest", "Integer");
        pPar.setUnit("m");

        final Property pChi = new Property("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest", "Integer");
        pChi.setUnit("m");

        pChi.addParent(pPar);

        final LinkedList<Entity> ret = getHeartDBClient().insertEntity(pPar, pChi);
        assertNotNull(ret);

        // two entities back
        assertEquals(2, ret.size());

        for (final Entity e : ret) {
            // both have no errors,
            assertNotNull(e.getMessages());
            assertEquals(0, e.getMessages().size());
            // ... have valid id,
            assertNotNull(e.getId());
            // ... and name,
            assertNotNull(e.getName());
            // ... and type,
            assertNotNull(e.getType());
            assertEquals(INTEGER, e.getType());
            // ... and description,
            assertNotNull(e.getDescription());
            // ... and unit,
            assertNotNull(e.getUnit());
            assertEquals("m", e.getUnit());
            // ... and are properties
            assertTrue(e instanceof Property);
        }

    }

    /**
     * The idea of the following test is: 1) take an integer property with unit
     * "m" 2) take a recordtype which has this integer property but overlays the
     * unit with unit "nm" 3) insert both
     * 
     * @author Timm Fitschen
     */
    @Test
    public void insertRecordWithPropertyWithUnitOverlay() {
        final Property p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest",
                "Integer");
        p.setUnit("m");
        p.setId(-1);

        final Property pClone = new Property(p.getName(), p.getDescription(), p.getType());
        pClone.setId(-1);
        pClone.setUnit("nm");

        final RecordType rt = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        rt.addProperty(pClone);

        LinkedList<Entity> ret = getHeartDBClient().insertEntity(p, rt);
        assertNotNull(ret);

        // two entities back
        assertEquals(2, ret.size());

        for (int i = 0; i <= 1; i++) {
            Integer rtId = null;
            Integer pId = null;
            for (final Entity e : ret) {
                // both have no errors,
                assertNotNull(e.getMessages());
                assertEquals(0, e.getMessages().size());
                // ... have valid id,
                assertNotNull(e.getId());
                // ... and name,
                assertNotNull(e.getName());
                // ... and description,
                assertNotNull(e.getDescription());
                if (e instanceof Property) {
                    assertEquals("m", e.getUnit());
                    pId = e.getId();
                    assertTrue(pId >= 100);
                } else if (e instanceof RecordType) {
                    assertNotNull(e.getProperties());
                    assertEquals(1, e.getProperties().size());
                    assertEquals("nm", e.getProperties().getFirst().getUnit());
                    rtId = e.getId();
                    assertTrue(rtId >= 100);
                } else {
                    Assert.fail("Returned entity was neither a Property nor a Record.");
                }
            }

            if (i == 0) {
                ret = getHeartDBClient().retrieveEntity(pId, rtId);
            }

        }

    }

    /**
     * Idea of this test: Insert two Reference Properties p1 and p2 which
     * reference two RecordTypes rt1 and rt2 which are to be added in one go.
     * The crucial point is that both RecordTypes are referencing each other by
     * implementing p1 and p2. Therefore a circular dependency is set up. This
     * test tests the ideal case in which everything is fine and should finally
     * be inserted. See next test for the corrupted case.
     * 
     * @author Timm Fitschen
     */

    @Test
    public void insertGoodLoopReferencingRecordTypes() {

        final RecordType rt1 = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        rt1.setId(-1);
        final RecordType rt2 = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        rt2.setId(-2);

        final Property p1 = new Property("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest", "Reference");
        p1.setReferenceTypeID(rt1.getId());
        final Property p2 = new Property("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest", "Reference");
        p2.setReferenceTypeID(rt2.getId());

        rt1.addProperty(p2);
        rt2.addProperty(p1);

        final LinkedList<Entity> ret = getHeartDBClient().insertEntity(rt1, rt2, p1, p2);
        assertNotNull(ret);

        // four entities back
        assertEquals(4, ret.size());

        for (final Entity e : ret) {
            // all four do not have any errors,
            assertNotNull(e.getMessages());
            assertEquals(0, e.getMessages().size());
            // ... have valid id,
            assertNotNull(e.getId());
            // ... and name,
            assertNotNull(e.getName());
            // ... and description,
            assertNotNull(e.getDescription());
        }
    }

    /**
     * Idea of this test: Insert two Reference Properties p1 and p2 which
     * reference two RecordTypes rt1 and rt2 which are to be added in one go.
     * The crucial point is that both RecordTypes are referencing each other by
     * implementing p1 and p2. Therefore a circular dependency is set up. This
     * test tests the ideal case in which everything is fine and should finally
     * be inserted. See next test for the corrupted case.
     * 
     * @author Timm Fitschen
     */

    @Test
    public void insertBadLoopReferencingRecordTypes() {

        final RecordType rt1 = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        rt1.setCuid(1);
        // here is the difference to the previous test
        // insertGoodLoopReferencingRecordTypes()
        // rt1.setId(-1);
        final RecordType rt2 = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        rt2.setId(-2);
        rt2.setCuid(2);

        final Property p1 = new Property("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest", "Reference");
        // this refid points on nothing
        p1.setReferenceTypeID(-1);
        p1.setCuid(3);
        final Property p2 = new Property("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest", "Reference");
        p2.setReferenceTypeID(rt2.getId());
        p2.setCuid(4);

        rt1.addProperty(p2);
        rt2.addProperty(p1);

        final LinkedList<Entity> ret = getHeartDBClient().insertEntity(rt1, rt2, p1, p2);
        assertNotNull(ret);

        // four entities back
        assertEquals(4, ret.size());

        for (final Entity e : ret) {
            // all four should have errors,
            assertNotNull(e.getMessages());
            assertTrue(1 >= e.getMessages().size());
            // ... have invalid id,
            if (e.getId() != null) {
                assertTrue(e.getId() < 0);
            }
        }
    }

    @Ignore
    @Test
    public void insertPropertyWithSubsubproperty() {
        final Property p1 = new Property("HeartDBClientTest_insertPropertyWithSubsubproperty_"
                + getRandString(), "HeartDBClientTest", "Text");
        p1.setCuid(1);
        Property p2 = new Property("HeartDBClientTest_insertPropertyWithSubsubproperty_"
                + getRandString(), "HeartDBClientTest", "Text");
        p2.setCuid(2);
        Property p3 = new Property("HeartDBClientTest_insertPropertyWithSubsubproperty_"
                + getRandString(), "HeartDBClientTest", "Text");
        p3.setCuid(3);

        LinkedList<Entity> ret = getHeartDBClient().insertEntity(p2, p3);

        for (final Entity p : ret) {
            p.print();
            if (p.getCuid() == 2) {
                p2 = (Property) p;
            }
            if (p.getCuid() == 3) {
                p3 = (Property) p;
            }
        }
        p2.addProperty(p3);
        p1.addProperty(p2);
        ret = getHeartDBClient().insertEntity(p1);

        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        final int id = ret.getFirst().getId();
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());

        assertEquals(TEXT, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // retrieve it once again
        ret = getHeartDBClient().retrieveEntity(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst());
        assertEquals(p2.getId(), ret.getFirst().getProperties().getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst());
        assertEquals(p3.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getId());

    }

    @Ignore
    @Test
    public void updatePropertyWithSubsubproperty() throws InterruptedException {
        final Property p1 = new Property("HeartDBClientTest_updatePropertyWithSubsubproperty_"
                + getRandString(), "HeartDBClientTest", "Text");
        p1.setCuid(1);
        Property p2 = new Property("HeartDBClientTest_updatePropertyWithSubsubproperty_"
                + getRandString(), "HeartDBClientTest", "Text");
        p2.setCuid(2);
        Property p3 = new Property("HeartDBClientTest_updatePropertyWithSubsubproperty_"
                + getRandString(), "HeartDBClientTest", "Text");
        p3.setCuid(3);
        Property p4 = new Property("HeartDBClientTest_updatePropertyWithSubsubproperty_"
                + getRandString(), "HeartDBClientTest", "Text");
        p4.setCuid(4);
        Property p5 = new Property("HeartDBClientTest_updatePropertyWithSubsubproperty_"
                + getRandString(), "HeartDBClientTest", "Text");
        p5.setCuid(5);

        LinkedList<Entity> ret = getHeartDBClient().insertEntity(p2, p3, p4, p5);

        for (final Entity p : ret) {
            p.print();
            if (p.getCuid() == 2) {
                p2 = (Property) p;
            }
            if (p.getCuid() == 3) {
                p3 = (Property) p;
            }
            if (p.getCuid() == 4) {
                p4 = (Property) p;
            }
            if (p.getCuid() == 5) {
                p5 = (Property) p;
            }
        }
        p3.addProperty(p4);
        p2.addProperty(p3);
        p1.addProperty(p2);
        ret = getHeartDBClient().insertEntity(p1);

        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        final int id = ret.getFirst().getId();
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());

        assertEquals(TEXT, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // retrieve it once again
        ret = getHeartDBClient().retrieveEntity(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst());
        assertEquals(p2.getId(), ret.getFirst().getProperties().getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst());
        assertEquals(p3.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst()
                .getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().getFirst()
                .getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst()
                .getProperties().getFirst());
        assertEquals(p4.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getProperties().getFirst().getId());

        // change subsubproperty from p4 to p5
        ret.getFirst().getProperties().getFirst().getProperties().getFirst().getProperties()
                .removeFirst();
        ret.getFirst().getProperties().getFirst().getProperties().getFirst().addProperty(p5);
        ret = getHeartDBClient().updateEntity(ret.getFirst());
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst());
        assertEquals(p2.getId(), ret.getFirst().getProperties().getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst());
        assertEquals(p5.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getProperties().getFirst().getId());

        ret = getHeartDBClient().retrieveEntity(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertEquals("TEXT", ret.getFirst().getType().toString().toUpperCase());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst());
        assertNotNull(ret.getFirst().getProperties().getFirst().getType());
        assertEquals("TEXT", ret.getFirst().getProperties().getFirst().getType().toString()
                .toUpperCase());
        assertEquals(p2.getId(), ret.getFirst().getProperties().getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst());
        assertEquals(p5.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getProperties().getFirst().getId());
    }

    @Ignore
    @Test
    public void updatePropertyWithSubsubpropertyAndDelete() throws InterruptedException {
        final Property p1 = new Property(
                "HeartDBClientTest_updatePropertyWithSubsubpropertyAndDelete_" + getRandString(),
                "HeartDBClientTest", "Text");
        p1.setCuid(1);
        Property p2 = new Property("HeartDBClientTest_updatePropertyWithSubsubpropertyAndDelete_"
                + getRandString(), "HeartDBClientTest", "Text");
        p2.setCuid(2);
        Property p3 = new Property("HeartDBClientTest_updatePropertyWithSubsubpropertyAndDelete_"
                + getRandString(), "HeartDBClientTest", "Text");
        p3.setCuid(3);
        Property p4 = new Property("HeartDBClientTest_updatePropertyWithSubsubpropertyAndDelete_"
                + getRandString(), "HeartDBClientTest", "Text");
        p4.setCuid(4);
        Property p5 = new Property("HeartDBClientTest_updatePropertyWithSubsubpropertyAndDelete_"
                + getRandString(), "HeartDBClientTest", "Text");
        p5.setCuid(5);

        LinkedList<Entity> ret = getHeartDBClient().insertEntity(p2, p3, p4, p5);

        for (final Entity p : ret) {
            p.print();
            if (p.getCuid() == 2) {
                p2 = (Property) p;
            }
            if (p.getCuid() == 3) {
                p3 = (Property) p;
            }
            if (p.getCuid() == 4) {
                p4 = (Property) p;
            }
            if (p.getCuid() == 5) {
                p5 = (Property) p;
            }
        }
        p3.addProperty(p4);
        p2.addProperty(p3);
        p1.addProperty(p2);
        ret = getHeartDBClient().insertEntity(p1);

        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        final int id = ret.getFirst().getId();
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());

        assertEquals(TEXT, ret.getFirst().getType());
        assertTrue(ret.getFirst() instanceof Property);

        // retrieve it once again
        ret = getHeartDBClient().retrieveEntity(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst());
        assertEquals(p2.getId(), ret.getFirst().getProperties().getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst());
        assertEquals(p3.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst()
                .getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().getFirst()
                .getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst()
                .getProperties().getFirst());
        assertEquals(p4.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getProperties().getFirst().getId());

        // change subsubproperty from p4 to p5
        ret.getFirst().getProperties().getFirst().getProperties().getFirst().getProperties()
                .removeFirst();
        ret.getFirst().getProperties().getFirst().getProperties().getFirst().addProperty(p5);
        ret = getHeartDBClient().updateEntity(ret.getFirst());
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst());
        assertEquals(p2.getId(), ret.getFirst().getProperties().getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst());
        assertEquals(p5.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getProperties().getFirst().getId());

        ret = getHeartDBClient().retrieveEntity(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertEquals("TEXT", ret.getFirst().getType().getType().toUpperCase());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst());
        assertEquals(p2.getId(), ret.getFirst().getProperties().getFirst().getId());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties());
        assertFalse(ret.getFirst().getProperties().getFirst().getProperties().isEmpty());
        assertNotNull(ret.getFirst().getProperties().getFirst().getProperties().getFirst());
        assertEquals(p5.getId(), ret.getFirst().getProperties().getFirst().getProperties()
                .getFirst().getProperties().getFirst().getId());

        final LinkedList<Property> del = getHeartDBClient().deleteProperty(
                new Integer[] { id, p2.getId(), p3.getId(), p4.getId(), p5.getId() });
        assertNotNull(del);
        assertEquals(5, ret.size());
        for (final Property p : del) {
            assertEquals("This entity has been deleted.", p.getMessages().getFirst()
                    .getDescription());
        }

    }

    @After
    public void writeLog() throws IOException {
        HeartDBClientTest.printLog();
    }

    // TODO
    public void testPropertyDuplicates() {

    }

}
