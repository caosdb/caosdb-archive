/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package caosdb.server;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.data.ServerInfo;
import org.restlet.engine.Engine;
import org.restlet.routing.Router;
import org.restlet.routing.Template;

import caosdb.server.CaosDBServerConnectorHelper;
import caosdb.server.database.DatabaseSessionManager;
import caosdb.server.datatype.AbstractDatatype;
import caosdb.server.resource.DeliverCSS;
import caosdb.server.resource.DeliverFile;
import caosdb.server.resource.DeliverXSL;
import caosdb.server.resource.Default.DefaultResource;
import caosdb.server.resource.Entity.EntityResource;
import caosdb.server.resource.Files.FileSystemResource;
import caosdb.server.resource.Files.FilesPickUpResource;
import caosdb.server.resource.Files.FilesResource;
import caosdb.server.resource.Files.ThumbnailsResource;
import caosdb.server.resource.Info.InfoResource;
import caosdb.server.resource.Property.PropertyResource;
import caosdb.server.resource.Query.QueryResource;
import caosdb.server.resource.RecordType.RecordTypeResource;
import caosdb.server.resource.Records.RecordsResource;
import caosdb.server.terminal.CaosDBTerminal;
import caosdb.server.utils.BenchmarkStats;
import caosdb.server.utils.LogFormatter;
import caosdb.server.utils.NullPrintStream;

public class CaosDBServer extends Application {
    private static Properties ServerProperties = null;
    private static Component component = null;

    public static Properties getServerProperties() {
        if (ServerProperties == null) {
            initServerProperties();
        }
        return ServerProperties;
    }

    /**
     * This init_server_properties method reads the config file which contains
     * key-value-pairs for such variables like the user name of the database,
     * the port the server will be listening on etc.
     */
    private static void initServerProperties() {
        ServerProperties = new Properties();
        try {
            final String basepath = System.getProperty("user.dir");
            ServerProperties.setProperty("basepath", basepath);
            ServerProperties.setProperty("file.policy", basepath + "/conf/policy.conf");
            ServerProperties.setProperty("file.messages", basepath + "/conf/messages.conf");
            ServerProperties.setProperty("POLICY_COMPONENT", "enabled");
            ServerProperties.setProperty("contextroot", "/mpidsserver");
            final File confFile = new File(basepath + "/conf/server.conf");
            if (confFile.exists()) {
                final BufferedInputStream sp_in = new BufferedInputStream(new FileInputStream(
                        confFile));
                ServerProperties.load(sp_in);
                sp_in.close();
            } else {
                System.out.println("No config file found. Will use standard configuration.");
            }

            // this is due to the userfolders must have a users name...
            if (ServerProperties.getProperty("userfolders").equalsIgnoreCase("true")) {
                ServerProperties.setProperty("POLICY_COMPONENT", "enabled");
            }
        } catch (final IOException e) {
            System.out.println("Failed to read config file. Will use standard configuration.");
        }
    }

    /**
     * This main method starts up a web application that will listen on a port
     * defined in the config file.
     * 
     * @param args
     *            One option temporarily (for testing) available: - quiet: If
     *            present: disable System.out-stream (stream to a
     *            NullPrintStream). This makes the response of the database
     *            amazingly faster.
     * @throws Exception
     *             If problems occur.
     */
    public static void main(final String[] args) {
        if (args.length < 1 || !args[0].equals("silent")) {
            final CaosDBTerminal caosDBTerminal = new CaosDBTerminal();
            caosDBTerminal.setName("CaosDBTerminal");
            caosDBTerminal.start();
        } else {
            System.setOut(new NullPrintStream());
        }
        BenchmarkStats.getInstance().start();
        try {
            Thread.sleep(1000);
            initServerProperties();
            AbstractDatatype.initializeDatatypes();
            final int port = Integer.parseInt(ServerProperties.getProperty("port", "8122"));
            final int initialConnections = Integer.parseInt(ServerProperties.getProperty(
                    "initialConnections", "10"));
            final int maxTotalConnections = Integer.parseInt(ServerProperties.getProperty(
                    "maxTotalConnections", "10"));
            runServer(port, initialConnections, maxTotalConnections);
        } catch (final Exception e) {
            e.printStackTrace();
            System.out.println("Failed to start Server.");
        }
    }

    /**
     * Starts a server running on the specified port. The context root will be
     * "/mpidsserver".
     * 
     * @author Timm Fitschen
     * @param port
     *            The port on which this server should run.
     * @throws Exception
     *             if problems occur starting up this server.
     */
    public static void runServer(final int port, final int initialConnections,
            final int maxTotalConnections) throws Exception {

        Engine.getInstance().getRegisteredServers().add(new CaosDBServerConnectorHelper(null));

        // Create a component.
        component = new HeartDBComponent();

        final Server httpServer = // new Server(Protocol.HTTP, port);
        new Server((Context) null, Arrays.asList(Protocol.HTTP), null, port, (Restlet) null,
                "caosdb.server.CaosDBServerConnectorHelper");
        component.getServers().add(httpServer);

        // set initial and maximal connections
        httpServer.getContext().getParameters()
                .add("initialConnections", Integer.toString(initialConnections));
        httpServer.getContext().getParameters()
                .add("maxTotalConnections", Integer.toString(maxTotalConnections));

        // Create an application (this class).
        final Application application = new CaosDBServer();
        // Attach the application to the component with a defined contextRoot.

        component.getDefaultHost().attach(ServerProperties.getProperty("contextroot"), application);

        component.getDefaultHost().attach("", DefaultResource.class)
                .setMatchingMode(Template.MODE_STARTS_WITH);
        final File logdir = new File("./log/");
        if (!logdir.exists()) {
            logdir.mkdir();
        }
        final Logger logger = Logger.getLogger("RequestLogger");
        logger.setLevel(Level.INFO);
        final FileHandler fh1 = new FileHandler("./log/request.log", false);
        fh1.setFormatter(new LogFormatter());
        logger.addHandler(fh1);
        final FileHandler fh2 = new FileHandler("./log/restlet.log", false);
        fh2.setFormatter(new SimpleFormatter());
        component.getLogger().addHandler(fh2);
        component.getLogger().setLevel(Level.INFO);

        component.start();

        if (component.isStarted()) {
            System.out.println("Server has started.");
        }
    }

    /**
     * Specify the dispatching restlet that maps URIs to their associated
     * resources for processing.
     * 
     * @return A Router restlet that implements dispatching.
     */
    @Override
    public Restlet createInboundRoot() {
        // Create a router restlet.
        final Router router = new Router(getContext());
        // Attach the resources to the router.
        router.attach("/Entities", EntityResource.class);
        router.attach("/Entities/", EntityResource.class);
        router.attach("/Entities/{specifier}", EntityResource.class);
        router.attach("/Entity", EntityResource.class);
        router.attach("/Entity/", EntityResource.class);
        router.attach("/Entity/{specifier}", EntityResource.class);
        router.attach("/RecordTypes", RecordTypeResource.class);
        router.attach("/RecordTypes/", RecordTypeResource.class);
        router.attach("/RecordTypes/{specifier}", RecordTypeResource.class);
        router.attach("/RecordType", RecordTypeResource.class);
        router.attach("/RecordType/", RecordTypeResource.class);
        router.attach("/RecordType/{specifier}", RecordTypeResource.class);
        router.attach("/Records", RecordsResource.class);
        router.attach("/Records/", RecordsResource.class);
        router.attach("/Records/{specifier}", RecordsResource.class);
        router.attach("/Record", RecordsResource.class);
        router.attach("/Record/", RecordsResource.class);
        router.attach("/Record/{specifier}", RecordsResource.class);
        router.attach("/Property", PropertyResource.class);
        router.attach("/Property/", PropertyResource.class);
        router.attach("/Property/{specifier}", PropertyResource.class);
        router.attach("/Properties", PropertyResource.class);
        router.attach("/Properties/", PropertyResource.class);
        router.attach("/Properties/{specifier}", PropertyResource.class);
        router.attach("/AbstractProperty", PropertyResource.class);
        router.attach("/AbstractProperty/", PropertyResource.class);
        router.attach("/AbstractProperty/{specifier}", PropertyResource.class);
        router.attach("/AbstractProperties", PropertyResource.class);
        router.attach("/AbstractProperties/", PropertyResource.class);
        router.attach("/AbstractProperties/{specifier}", PropertyResource.class);
        router.attach("/FilesDropOff", FilesPickUpResource.class);
        router.attach("/FilesDropOff/", FilesPickUpResource.class);
        router.attach("/FileSystem", FileSystemResource.class).setMatchingMode(
                Template.MODE_STARTS_WITH);
        router.attach("/Thumbnails/", ThumbnailsResource.class).setMatchingMode(
                Template.MODE_STARTS_WITH);
        router.attach("/Files", FilesResource.class);
        router.attach("/Files/", FilesResource.class);
        router.attach("/Files/{specifier}", FilesResource.class);
        router.attach("/File/", FilesResource.class);
        router.attach("/File", FilesResource.class);
        router.attach("/File/{specifier}", FilesResource.class);
        router.attach("/Info", InfoResource.class);
        router.attach("/Info/", InfoResource.class);
        router.attach("/record.xsl", DeliverXSL.class);
        router.attach("/record.css", DeliverCSS.class);
        router.attach("/webinterface/{file}", DeliverFile.class);
        router.attach("?{query}", QueryResource.class).setMatchingQuery(true);
        // router.attach("/Query", QueryResource.class);
        router.attachDefault(DefaultResource.class);
        return router;

    }

    private static ServerInfo serverInfo = null;

    public static ServerInfo getServerInfo() {
        if (serverInfo == null) {
            serverInfo = new ServerInfo();
            serverInfo.setAgent("CaosDB Server");
        }
        return serverInfo;
    }

    public static void shutDown() {
        // stopServer - no new connections will be accepted.
        System.err.print("\nStopping HTTP Server. Waiting for all connections to be finished...");
        try {
            component.stop();
            System.err.print(" [OK]\n");
        } catch (final Exception e) {
            System.err.print(" [failed]\n");
            e.printStackTrace();
        }

        // stopMySQL connection pool
        System.err.print("\nStopping MySQL connection pool...");
        DatabaseSessionManager.stop();
        System.err.print(" [OK]\n");

        System.err.print("\n\nbye!\n");
        System.exit(0);
    }

    public static Component getComponent() {
        return component;
    }

}

class HeartDBComponent extends Component {

    public HeartDBComponent() {
        super();
        setName("CaosDB Server");
        setOwner("Biomedical Physics Group at the Max Planck Institute for Dynamics and Self-Organization in Göttingen, Germany");
        setAuthor("Developers of the Biomedical Physics Group at the Max Planck Institute for Dynamics and Self-Organization in Göttingen, Germany");
    }

    @Override
    public void handle(final Request request, final Response response) {
        response.setServerInfo(CaosDBServer.getServerInfo());
        super.handle(request, response);
    }
}
